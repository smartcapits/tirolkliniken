﻿namespace SmartCAP.CM.TK_Anästhesie
{
    partial class SetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupForm));
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.chkSetValidDocumentsValidated = new System.Windows.Forms.CheckBox();
            this.chkSkipValidationIfBatchIsValid = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(179, 66);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(92, 35);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(283, 66);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(62, 35);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // chkSetValidDocumentsValidated
            // 
            this.chkSetValidDocumentsValidated.AutoSize = true;
            this.chkSetValidDocumentsValidated.Checked = true;
            this.chkSetValidDocumentsValidated.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSetValidDocumentsValidated.Location = new System.Drawing.Point(11, 11);
            this.chkSetValidDocumentsValidated.Margin = new System.Windows.Forms.Padding(2);
            this.chkSetValidDocumentsValidated.Name = "chkSetValidDocumentsValidated";
            this.chkSetValidDocumentsValidated.Size = new System.Drawing.Size(206, 17);
            this.chkSetValidDocumentsValidated.TabIndex = 10;
            this.chkSetValidDocumentsValidated.Text = "Setzte gültige Dokumente auf Validiert";
            this.chkSetValidDocumentsValidated.UseVisualStyleBackColor = true;
            // 
            // chkSkipValidationIfBatchIsValid
            // 
            this.chkSkipValidationIfBatchIsValid.AutoSize = true;
            this.chkSkipValidationIfBatchIsValid.Location = new System.Drawing.Point(11, 33);
            this.chkSkipValidationIfBatchIsValid.Margin = new System.Windows.Forms.Padding(2);
            this.chkSkipValidationIfBatchIsValid.Name = "chkSkipValidationIfBatchIsValid";
            this.chkSkipValidationIfBatchIsValid.Size = new System.Drawing.Size(340, 17);
            this.chkSkipValidationIfBatchIsValid.TabIndex = 9;
            this.chkSkipValidationIfBatchIsValid.Text = "Überspringe Validierung wenn alle Dokumente im Stapel gültig sind";
            this.chkSkipValidationIfBatchIsValid.UseVisualStyleBackColor = true;
            // 
            // SetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 111);
            this.Controls.Add(this.chkSetValidDocumentsValidated);
            this.Controls.Add(this.chkSkipValidationIfBatchIsValid);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.Name = "SetupForm";
            this.Text = "SmartCAP.CM.TK_Anästhesie.Setup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox chkSetValidDocumentsValidated;
        private System.Windows.Forms.CheckBox chkSkipValidationIfBatchIsValid;
    }
}