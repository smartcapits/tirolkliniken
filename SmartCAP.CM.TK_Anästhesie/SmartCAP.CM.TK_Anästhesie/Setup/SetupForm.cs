﻿using Kofax.Capture.AdminModule.InteropServices;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;

namespace SmartCAP.CM.TK_Anästhesie
{
    public partial class SetupForm : Form
    {
        #region Initialization
        public IBatchClass BatchClass;
        public SetupForm()
        {
            InitializeComponent();
        }

        public DialogResult ShowDialog(IBatchClass batchClass)
        {
            BatchClass = batchClass;
            InitializePanel();
            LoadSettings();

            return this.ShowDialog();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (VerifySettings())
            {
                SaveSettings();
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion


        public void InitializePanel()
        {
        }

        public void LoadSettings()
        {
            // Load all CustomProperties into controls
            GetAll(this, typeof(CheckBox)).ToList().ForEach(x => (x as CheckBox).Checked = bool.Parse(GetCustomStorageString("SmartCAP.CM.TK_Anästhesie." + x.Name, false.ToString())));
        }

        public bool VerifySettings()
        {
            return true;
        }

        public void SaveSettings()
        {
            // Save all control values to CustomProperties named after the control name
            GetAll(this, typeof(CheckBox)).ToList().ForEach(x => SetCustomStorageString("SmartCAP.CM.TK_Anästhesie." + x.Name, (x as CheckBox).Checked.ToString()));
        }

        #region helpers
        /// <summary>
        /// Gets all controls of the specified type recursive (e.g. if you use textboxes inside groupboxes)
        /// </summary>
        /// <param name="control"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
        private void SetCustomStorageString(string name, string value)
        {
            try
            { BatchClass.set_CustomStorageString(name, value); }
            catch
            { }
        }

        private string GetCustomStorageString(string name, string defaultValue = "")
        {
            try
            { return BatchClass.get_CustomStorageString(name); }
            catch
            { return defaultValue; }
        }

        #endregion

    }
}
