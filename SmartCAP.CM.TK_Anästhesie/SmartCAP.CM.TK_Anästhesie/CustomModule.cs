﻿using Kofax.Capture.DBLite;
using Kofax.Capture.SDK.CustomModule;
using Kofax.Capture.SDK.Data;
using log4net;
using Microsoft.Win32;
using SmartCAP.Logging;
using SmartCAP.SmartKCBatch;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace SmartCAP.CM.TK_Anästhesie
{
    #region event handlers
    public class BatchEventArgs : EventArgs
    {
        public IBatch Batch { get; private set; }
        public BatchEventArgs(IBatch batch)
        {
            Batch = batch;
        }
    }
    public class ACDataElementEventArgs : EventArgs
    {
        public IACDataElement Element { get; private set; }

        public ACDataElementEventArgs(IACDataElement element)
        {
            Element = element;
        }
    }
    public class TextEventArgs : EventArgs
    {
        public string Text { get; private set; }

        public TextEventArgs(string text)
        {
            Text = text;
        }
    }

    public class DocumentEventArgs : EventArgs
    {
        public Document Document { get; private set; }

        public DocumentEventArgs(Document document)
        {
            Document = document;
        }
    }

    public class ProgressBarEventArgs : EventArgs
    {
        public int MinValue { get; private set; }
        public int MaxValue { get; private set; }
        public int CurrentValue { get; private set; }

        public ProgressBarEventArgs(int minValue = -1, int maxValue = -1, int currentValue  =-1)
        {
            MinValue = minValue;
            MaxValue = maxValue;
            CurrentValue = currentValue;
        }
    }

    public delegate void BatchEventHandler(object sender, BatchEventArgs e);
    //public delegate void ACDataElementEventHandler(object sender, ACDataElementEventArgs e);
    public delegate void TextEventHandler(object sender, TextEventArgs e);
    public delegate void DocumentEventHandler(object sender, DocumentEventArgs e);
    public delegate void ProgressBarHandler(object sender, ProgressBarEventArgs e);
    #endregion event handlers

    public class CustomModule
    {
        #region log4net
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private void InitializeLogging()
        {
            try
            {
                LoggerFunctions.Initialize(this.GetType().Namespace);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not initialize logging: " + ex.Message);
            }
        }
        #endregion        

        public static bool AllowMultiInstance = true;
        public static string ServiceName = "SmartCAP.CM.TK_Anästhesie";
        public static string DisplayName = "SmartCAP.CM.TK_Anästhesie Service";
        public static string Description = "Kofax Capture Service for Custom Module SmartCAP.CM.TK_Anästhesie";
        /// <summary>
        /// This setting is only used, if batch notification is disabled
        /// </summary>
        private static double pollIntervalSeconds = 60;

        #region initialization and login

        private Login login;
        private IRuntimeSession session;
        private IBatch activeBatch;
        private Mutex mutex;
        private KfxDbFilter filter = KfxDbFilter.KfxDbFilterOnProcess | KfxDbFilter.KfxDbFilterOnStates | KfxDbFilter.KfxDbSortOnPriorityDescending;
        private KfxDbState states = KfxDbState.KfxDbBatchReady;
        private System.Timers.Timer pollTimer = new System.Timers.Timer();

        public event BatchEventHandler BatchOpened;
        public event BatchEventHandler BatchClosed;
        public event DocumentEventHandler DocumentOpened;
        public event DocumentEventHandler DocumentClosed;
        public event TextEventHandler ErrorOccured;
        public event TextEventHandler LogEvent;
        public event ProgressBarHandler ProgressBarUpdate;

        /// <summary>
        /// Returns the batch notification state (existence of DisableBatchNotification registry key)
        /// </summary>
        public static bool BatchNotificationEnabled { get { return GetBatchNotificationState(); } }
        private static bool GetBatchNotificationState()
        {
            string registryKey;
            if (Environment.Is64BitOperatingSystem)
                registryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0";
            else
                registryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Kofax Image Products\Ascent Capture\3.0";

            string value = Convert.ToString(Registry.GetValue(registryKey, "DisableBatchNotification", ""));

            if (String.IsNullOrEmpty(value))
            {
                // DisableBatchNotification key does not exist - batch notification is enabled
                return true;
            }
            else
            {
                // DisableBatchNotificationKey exists - batch notification is disabled
                return false;

            }
        }

        /// <summary>
        /// Provides all necessary methods for batch processing
        /// </summary>
        public CustomModule()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);
            InitializeLogging();
            mutex = new Mutex();
        }

        /// <summary>
        ///  Login to KC
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        public void Login(string user, string password, bool showDialog = false)
        {
            login = new Login();
            try
            {
                login.EnableSecurityBoost = true;
                login.Login();
                login.ApplicationName = "SmartCAP.CM.TK_Anästhesie";
                //login.Version = "1.0";            
                login.ValidateUser("SmartCAP.CM.TK_Anästhesie.exe", showDialog, user, password);

                session = login.RuntimeSession;
            }
            catch (Exception ex)
            {
                string message = "Error logging in to KC: " + ex.ToString();
                Log.Error(message);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
                throw;
            }
        }

        /// <summary>
        /// The CustomModule will be notified from Kofax Capture, if new batches are available. 
        /// </summary>
        public void ListenForNewBatches()
        {
            if (session == null)
                throw new Exception("you need to login to KC first");

            session.BatchAvailable += Session_BatchAvailable;
        }

        /// <summary>
        /// Polls for new batches
        /// </summary>
        /// <param name="interval">poll interval in milliseconds</param>
        public void PollForNewBatches()
        {
            pollTimer.Interval = pollIntervalSeconds * 1000;
            pollTimer.Elapsed += PollTimer_Elapsed;
            pollTimer.Enabled = true;
        }

        #endregion initialization and login

        /// <summary>
        /// Processes the currently active batch.
        /// Here all the custom magic happens.
        /// </summary>
        private void ProcessActiveBatch()
        {
            string rxPatientennummer = "";
            string rxFallzahl = "";
            bool batchNeedsValidation = false;

            try
            {
                BatchOpened?.Invoke(this, new BatchEventArgs(activeBatch));

                Log.Debug("Initialize SmartBatch");
                SmartCMBatch smartBatch = new SmartCMBatch(activeBatch);

                Log.Debug("Loading settings");
                if (smartBatch.BatchFields.Any(bF => bF.Name == "RegEx_Patientennummer")) { rxPatientennummer = smartBatch.BatchFields.First(bF => bF.Name == "RegEx_Patientennummer").Value; }
                else { throw new Exception("Das Stapelfald 'RegEx_Patientennummer' ist nicht vorhanden"); }
                if (smartBatch.BatchFields.Any(bF => bF.Name == "RegEx_Fallzahl")) { rxFallzahl = smartBatch.BatchFields.First(bF => bF.Name == "RegEx_Fallzahl").Value; }
                else { throw new Exception("Das Stapelfald 'RegEx_Fallzahl' ist nicht vorhanden"); }
                if (!smartBatch.Setup.BatchClass.SetupCustomStorageStrings.Any(css => css.Name.Contains("SmartCAP.CM.TK_Anästhesie"))) { throw new Exception("Bitte das Setup für das CumstomModule 'SmartCAP.CM.TK_Anästhesie' durchführen"); }


                // Special handling of Documenttype "ANA Postop-Intensiv-Protokoll" (only separate when Patientennummer is found)
                if (smartBatch.BatchFields.First(bF => bF.Name == "Dokumententyp").Value.ToLower() == "ANA Postop-Intensiv-Protokoll".ToLower())
                {
                    LogMessage("Special handling of ANA Postop-Intensiv-Protokoll");

                    Document targetDoc = null;
                    // Only separate documents when Patienrennummer is resent -> attach documents that only provide Fallzahl to previous document
                    smartBatch.Documents.ForEach(doc =>
                    {
                        if (doc.OrderNumber == 0 || doc.Pages[0].PageLevelBarcodes.Any(bc => Regex.IsMatch(bc.Value, rxPatientennummer))) { targetDoc = doc; }
                        else if (doc.Pages.Count > 0 && !doc.Pages[0].PageLevelBarcodes.Any(bc => Regex.IsMatch(bc.Value, rxPatientennummer)))
                        {
                            LogMessage("Combining document {0} (no Patientennummer) with document {1}".Replace("{0}",doc.OrderNumber.ToString()).Replace("{1}", targetDoc.OrderNumber.ToString()));
                            doc.Pages.ForEach(p => p.MoveToDocument(targetDoc));
                            doc.Delete();
                        }
                        else if (doc.Pages.Count == 0) { doc.Delete(); }
                    });
                    
                    // Combine all documents with same Patientennummer into one document
                    smartBatch.Documents.ForEach(d =>
                    {
                        if (d.Pages.Count > 0 && d.Pages[0].PageLevelBarcodes.Any(bc => Regex.IsMatch(bc.Value, rxPatientennummer)))
                        {
                            string patientenNummer = d.Pages[0].PageLevelBarcodes.First(bc => Regex.IsMatch(bc.Value, rxPatientennummer)).Value;
                            
                            // Check the batch for a document that has pages, has a barcode that has the same value as the barcode on d fist page and also has a lower order number and hence is before d
                            Document targetDocument = smartBatch.Documents.FirstOrDefault(doc => doc.Pages.Count > 0 && doc.Pages[0].PageLevelBarcodes.Any(bc => bc.Value == patientenNummer && doc.OrderNumber < d.OrderNumber));
                            if (targetDocument != null)
                            {
                                LogMessage("Merging document {0} (Patientennummer: {1}) with document {2}".Replace("{0}", d.OrderNumber.ToString()).Replace("{1}", patientenNummer).Replace("{2}", targetDoc.OrderNumber.ToString()));
                                d.Pages.ForEach(p => p.MoveToDocument(targetDocument));
                                d.Delete();
                            }
                        }
                    });
                }
                // Special handling of Documenttype "ANA OP-Einverständniserklärung" (separate after every 2 pages - front and back page)
                else if (smartBatch.BatchFields.First(bF => bF.Name == "Dokumententyp").Value.ToLower() == "ANA OP-Einverstaendniserklaerung".ToLower())
                {
                    LogMessage("Special handling of ANA OP-Einverstaendniserklaerung");

                    // Move all pages to batch level
                    smartBatch.Documents.ForEach( doc => 
                    {
                        doc.Pages.ForEach(p => p.MoveToBatch(smartBatch));
                        doc.Delete();
                    });
                        
                    // Create a new document for every 2 pages
                    Document targetDoc = null;
                    int pageNum = 0;
                    smartBatch.LoosePages.ForEach(p =>
                    {
                        if (pageNum % 2 == 0) { targetDoc = smartBatch.CreateDocument(smartBatch.Setup.DocumentClasses[0].FormTypes[0].Name); }
                        p.MoveToDocument(targetDoc);
                        pageNum++;
                    });
                }


                // Updates the progress bar to our new count of documents
                UpdateProgressBar(-1, -1, smartBatch.Documents.Count);

                // Detect Barcodes
                smartBatch.Documents.ForEach(d =>
                {
                    Log.Debug("This message is logged to log4net as configured"); // log to log4net                    
                    LogDocumentOpened(d); // Log the document opening to the event list and increases the document progress bar, if the custom module is executed in interactive mode                    

                    // Find Pateintennummer and Fallzahl in documents first pages Barcodes and assign values to IndexFields
                    if (d.Pages.Count > 0)
                    {
                        Log.Debug("´Searching PageLevelBarcodes of page 0 for Patientennummer and Fallzahl");
                        if (d.Pages[0].PageLevelBarcodes.Any(bc => Regex.IsMatch(bc.Value, rxPatientennummer)))
                        {
                            d.IndexFields.First(iF => iF.Name == "Patientennummer").Value = d.Pages[0].PageLevelBarcodes.First(bc => Regex.IsMatch(bc.Value, rxPatientennummer)).Value;
                            LogMessage("Found Pateintennummer: " + d.IndexFields.First(iF => iF.Name == "Patientennummer").Value);
                        }
                        else
                        { LogMessage("Patientennummer not found!"); }

                        if (d.Pages[0].PageLevelBarcodes.Any(bc => Regex.IsMatch(bc.Value, rxFallzahl)))
                        {
                            d.IndexFields.First(iF => iF.Name == "Fallzahl").Value = d.Pages[0].PageLevelBarcodes.First(bc => Regex.IsMatch(bc.Value, rxFallzahl)).Value;
                            LogMessage("Found Fallzahl: " + d.IndexFields.First(iF => iF.Name == "Fallzahl").Value);
                        }
                        else
                        { LogMessage("Fallzahl not found!"); }
                    }

                    // Set document validated if option is set and both barcodes were found
                    if (bool.Parse(smartBatch.Setup.BatchClass.SetupCustomStorageStrings.First(css => css.Name.Contains("SmartCAP.CM.TK_Anästhesie.chkSetValidDocumentsValidated")).Value) &&
                        d.IndexFields.First(iF => iF.Name == "Patientennummer").Value != "" && d.IndexFields.First(iF => iF.Name == "Fallzahl").Value != "")
                    {
                        Log.Debug("Setting document validated");
                        d.Validated = true;
                        d.Note = "";
                    }
                    else if (d.IndexFields.First(iF => iF.Name == "Patientennummer").Value == "" || d.IndexFields.First(iF => iF.Name == "Fallzahl").Value == "")
                    {
                        Log.Debug("Setting document unvalidated");
                        d.Validated = false;
                        d.Note = "Mind. ein Indexwert muss ergänzt werden";
                        batchNeedsValidation = true;
                    }

                    LogDocumentClosed(d); // Log the document closing to the event list, if the custom module is executed in interactive mode
                });

                Log.Debug("Closing batch");
                if (bool.Parse(smartBatch.Setup.BatchClass.SetupCustomStorageStrings.First(css => css.Name.Contains("SmartCAP.CM.TK_Anästhesie.chkSkipValidationIfBatchIsValid")).Value) && 
                    !batchNeedsValidation && smartBatch.Modules.Any(m => m.ID == "index.exe"))
                { smartBatch.Close(KfxDbState.KfxDbBatchReady, smartBatch.Modules.SkipWhile(m => m.ID != "index.exe").Skip(1).First()); }
                else
                { smartBatch.Close(KfxDbState.KfxDbBatchReady, KfxDbQueue.KfxDbQueueNext, ""); }

                smartBatch = null;
                BatchClosed?.Invoke(this, new BatchEventArgs(activeBatch));
            }
            catch (Exception ex)
            {
                string message = "Error processing batch: " + ex.ToString();
                Log.Error(message, ex);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
                if (activeBatch != null)
                {
                    activeBatch.BatchClose(KfxDbState.KfxDbBatchError, KfxDbQueue.KfxDbQueueException, 1000, ex.ToString());
                    BatchClosed?.Invoke(this, new BatchEventArgs(activeBatch));
                }
            }
        }

        #region processing methods                  

        /// <summary>
        /// This function handles the BatchAvailable event from Kofax Capture. 
        /// This handler attempts to process as many batches as possible.
        /// A mutex is implemented to ensure that this logic is processed by only one thread at a time.
        /// </summary>
        private void Session_BatchAvailable()
        {
            Log.Debug("Session_BatchAvailable");
            // lock access to this call
            mutex.WaitOne();

            ProcessBatches();

            // release the kraken - ah, mutex
            mutex.ReleaseMutex();
        }

        /// <summary>
        /// This function processes all new batches whenever the poll timer elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Log.Debug("PollTimer_Elapsed");

            // lock access to this call
            mutex.WaitOne();

            ProcessBatches();

            // release the kraken - ah, mutex
            mutex.ReleaseMutex();
        }

        /// <summary>
        /// Processes only one specific batch.
        /// </summary>
        /// <param name="batchId"></param>
        public void ProcessSpecificBatch(int batchId)
        {
            try
            {
                activeBatch = session.BatchOpen(batchId, session.ProcessID);
                Log.Info("opened specific batch with id " + batchId);
                ProcessActiveBatch();
            }
            catch (Exception ex)
            {
                string message = "Error opening batch: " + ex.ToString();
                Log.Error(message);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
            }

        }

        /// <summary>
        /// Automatically gets the next batch and processes it.
        /// Returns true, if the batch was processed successful and false, if there are no more batches or an error occured.
        /// </summary>
        /// <returns></returns>
        private bool ProcessNextBatch()
        {
            try
            {
                activeBatch = session.NextBatchGet(login.ProcessID, filter, states);
                if (activeBatch == null)
                {
                    Log.Info("no more batches available at the moment");
                    return false;
                }
                else
                {
                    Log.Info("opened next batch with id " + activeBatch.BatchId);
                    // here happens the action
                    ProcessActiveBatch();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string message = "Error opening next batch: " + ex.ToString();
                // suppress duplicate key error
                if (!ex.Message.Contains("Duplicate key value supplied."))
                {
                    Log.Error(message, ex);
                    ErrorOccured?.Invoke(this, new TextEventArgs(message));
                }
                else
                {
                    Log.Error("Suppressed error: " + message, ex);
                }
                return false;
            }
        }

        /// <summary>
        /// Automatically processes all batches until there is no one left, or an error occured.
        /// </summary>
        public void ProcessBatches()
        {
            bool processedSuccessful = true;
            while (processedSuccessful)
            {
                processedSuccessful = ProcessNextBatch();
                //Log.Info("processed batch successfully.");
            }
        }

        #endregion login and processing methods

        #region helpers

        /// <summary>
        /// Get a list of all custom storage strings that were set during setup in administration
        /// </summary>
        /// <param name="runtime"></param>
        /// <param name="setup"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetSetupCustomStorageStrings(IACDataElement runtime, IACDataElement setup)
        {
            Dictionary<string, string> customStorageStrings = new Dictionary<string, string>();
            IACDataElement batch = runtime.FindChildElementByName("Batch");
            IACDataElementCollection batchClasses = setup.FindChildElementByName("BatchClasses").FindChildElementsByName("BatchClass");
            foreach (IACDataElement batchClass in batchClasses)
            {
                if (batchClass["Name"] == batch["BatchClassName"])
                {
                    foreach (IACDataElement css in batchClass.FindChildElementByName("BatchClassCustomStorageStrings").FindChildElementsByName("BatchClassCustomStorageString"))
                    {
                        customStorageStrings.Add(css["Name"], css["Value"]);
                    }
                }
            }
            return customStorageStrings;
        }

        /// <summary>
        /// Logs a message to the event list, if the custom module is executed in interactive mode
        /// </summary>
        /// <param name="message"></param>
        private void LogMessage(string message)
        {
            LogEvent?.Invoke(this, new TextEventArgs(message));
        }

        /// <summary>
        /// Logs the document opening to the event list and increases the document progress bar, if the custom module is executed in interactive mode
        /// </summary>
        /// <param name="document"></param>
        private void LogDocumentOpened(Document document)
        {
            DocumentOpened?.Invoke(this, new DocumentEventArgs(document));
        }

        /// <summary>
        /// Logs the document closing to the event list, if the custom module is executed in interactive mode
        /// </summary>
        /// <param name="document"></param>
        private void LogDocumentClosed(Document document)
        {
            DocumentClosed?.Invoke(this, new DocumentEventArgs(document));
        }

        /// <summary>
        /// Updates the progress bar - in case the number of documents has changed
        /// </summary>
        /// <param name="currentValue"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        private void UpdateProgressBar(int currentValue = -1, int minValue = -1, int maxValue = -1)
        {
            ProgressBarUpdate?.Invoke(this, new ProgressBarEventArgs(minValue, maxValue, currentValue));
        }

        #endregion helpers
    }
}
