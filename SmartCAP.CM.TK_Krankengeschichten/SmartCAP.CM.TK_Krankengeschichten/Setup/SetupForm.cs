﻿using Kofax.Capture.AdminModule.InteropServices;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;

namespace SmartCAP.CM.TK_Krankengeschichten
{
    public partial class SetupForm : Form
    {
        #region Initialization
        public IBatchClass BatchClass;

        public SetupForm()
        {
            InitializeComponent();
          }

        public DialogResult ShowDialog(IBatchClass batchClass)
        {
            BatchClass = batchClass;
            InitializePanel();
            LoadSettings();

            return this.ShowDialog();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (VerifySettings())
            {
                SaveSettings();
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        public void InitializePanel()
        {
            // Load all available BatchFields into combo boxes
            foreach (BatchField bf in BatchClass.BatchFields)
            {
                cboBFRXPatientennummer.Items.Add(bf.Name);
                cboRXBFFallzahl.Items.Add(bf.Name);
                cboBFDokArtOETranslation.Items.Add(bf.Name);
            }
        }
        
        public void LoadSettings()
        {
            try
            {
                // load all CustomProperties into controls
                GetAll(this, typeof(ComboBox)).ToList().ForEach(x => x.Text = GetCustomStorageString("SmartCAP.CM.TK_Krankengeschichten." + x.Name));
                GetAll(this, typeof(CheckBox)).ToList().ForEach(x => (x as CheckBox).Checked = bool.Parse(GetCustomStorageString("SmartCAP.CM.TK_Krankengeschichten." + x.Name, false.ToString())));
            }
            catch (Exception)
            {}  // Do nothing, juist display what we´ve got
            
        }

        public bool VerifySettings()
        {
            return true;
        }

        public void SaveSettings()
        {
            // save all control values to CustomProperties named after the control name
            GetAll(this, typeof(ComboBox)).ToList().ForEach(x => SetCustomStorageString("SmartCAP.CM.TK_Krankengeschichten." + x.Name, x.Text));
            GetAll(this, typeof(CheckBox)).ToList().ForEach(x => SetCustomStorageString("SmartCAP.CM.TK_Krankengeschichten." + x.Name, (x as CheckBox).Checked.ToString()));
        }

        #region helpers
        /// <summary>
        /// Gets all controls of the specified type recursive (e.g. if you use textboxes inside groupboxes)
        /// </summary>
        /// <param name="control"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
        private void SetCustomStorageString(string name, string value)
        {
            try
            {
                BatchClass.set_CustomStorageString(name, value);
            }
            catch
            {
            }
        }

        private string GetCustomStorageString(string name, string defaultValue = "")
        {
            try
            {
                return BatchClass.get_CustomStorageString(name);
            }
            catch
            {
                return defaultValue;
            }
        }

        #endregion
    }
}
