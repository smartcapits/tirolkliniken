﻿using Kofax.Capture.AdminModule.InteropServices;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace SmartCAP.CM.TK_Krankengeschichten
{
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface ISetupForm
    {
        [DispId(1)]
        AdminApplication Application { set; }
        [DispId(2)]
        void ActionEvent(int EventNumber, object Argument, out int Cancel);
    }

    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("SmartCAP.CM.TK_Krankengeschichten.Setup")]
    public class CustomModuleSetup : UserControl, ISetupForm
    {
        private AdminApplication adminApplication;

        public AdminApplication Application
        {
            set
            {
                value.AddMenu("SmartCAP.CM.TK_Krankengeschichten.Setup", "SmartCAP.CM.TK_Krankengeschichten - Setup", "BatchClass");
                adminApplication = value;
            }
        }

        public void ActionEvent(int EventNumber, object Argument, out int Cancel)
        {
            Cancel = 0;

            if ((KfxOcxEvent)EventNumber == KfxOcxEvent.KfxOcxEventMenuClicked && (string)Argument == "SmartCAP.CM.TK_Krankengeschichten.Setup")
            {
                SetupForm form = new SetupForm();
                form.ShowDialog(adminApplication.ActiveBatchClass);
            }
        }

    }
}
