﻿namespace SmartCAP.CM.TK_Krankengeschichten
{
    partial class SetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupForm));
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chkSkipValidationIfBatchIsValid = new System.Windows.Forms.CheckBox();
            this.chkSetValidDocumentsValidated = new System.Windows.Forms.CheckBox();
            this.cboBFRXPatientennummer = new System.Windows.Forms.ComboBox();
            this.cboRXBFFallzahl = new System.Windows.Forms.ComboBox();
            this.cboBFDokArtOETranslation = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(179, 192);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(161, 46);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancel.Location = new System.Drawing.Point(346, 192);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(121, 46);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "RegEx Patientennummer:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "RegEx Fallzahl:";
            // 
            // chkSkipValidationIfBatchIsValid
            // 
            this.chkSkipValidationIfBatchIsValid.AutoSize = true;
            this.chkSkipValidationIfBatchIsValid.Location = new System.Drawing.Point(15, 155);
            this.chkSkipValidationIfBatchIsValid.Name = "chkSkipValidationIfBatchIsValid";
            this.chkSkipValidationIfBatchIsValid.Size = new System.Drawing.Size(452, 21);
            this.chkSkipValidationIfBatchIsValid.TabIndex = 6;
            this.chkSkipValidationIfBatchIsValid.Text = "Überspringe Validierung wenn alle Dokumente im Stapel gültig sind";
            this.chkSkipValidationIfBatchIsValid.UseVisualStyleBackColor = true;
            // 
            // chkSetValidDocumentsValidated
            // 
            this.chkSetValidDocumentsValidated.AutoSize = true;
            this.chkSetValidDocumentsValidated.Checked = true;
            this.chkSetValidDocumentsValidated.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSetValidDocumentsValidated.Location = new System.Drawing.Point(15, 128);
            this.chkSetValidDocumentsValidated.Name = "chkSetValidDocumentsValidated";
            this.chkSetValidDocumentsValidated.Size = new System.Drawing.Size(271, 21);
            this.chkSetValidDocumentsValidated.TabIndex = 7;
            this.chkSetValidDocumentsValidated.Text = "Setzte gültige Dokumente auf Validiert";
            this.chkSetValidDocumentsValidated.UseVisualStyleBackColor = true;
            // 
            // cboBFRXPatientennummer
            // 
            this.cboBFRXPatientennummer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBFRXPatientennummer.FormattingEnabled = true;
            this.cboBFRXPatientennummer.Location = new System.Drawing.Point(198, 21);
            this.cboBFRXPatientennummer.Name = "cboBFRXPatientennummer";
            this.cboBFRXPatientennummer.Size = new System.Drawing.Size(269, 24);
            this.cboBFRXPatientennummer.TabIndex = 8;
            // 
            // cboRXBFFallzahl
            // 
            this.cboRXBFFallzahl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRXBFFallzahl.FormattingEnabled = true;
            this.cboRXBFFallzahl.Location = new System.Drawing.Point(198, 53);
            this.cboRXBFFallzahl.Name = "cboRXBFFallzahl";
            this.cboRXBFFallzahl.Size = new System.Drawing.Size(269, 24);
            this.cboRXBFFallzahl.TabIndex = 9;
            // 
            // cboBFDokArtOETranslation
            // 
            this.cboBFDokArtOETranslation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBFDokArtOETranslation.FormattingEnabled = true;
            this.cboBFDokArtOETranslation.Location = new System.Drawing.Point(198, 85);
            this.cboBFDokArtOETranslation.Name = "cboBFDokArtOETranslation";
            this.cboBFDokArtOETranslation.Size = new System.Drawing.Size(269, 24);
            this.cboBFDokArtOETranslation.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "DokArt -> OE Mapping:";
            // 
            // SetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 250);
            this.Controls.Add(this.cboBFDokArtOETranslation);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cboRXBFFallzahl);
            this.Controls.Add(this.cboBFRXPatientennummer);
            this.Controls.Add(this.chkSetValidDocumentsValidated);
            this.Controls.Add(this.chkSkipValidationIfBatchIsValid);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SetupForm";
            this.Text = "SmartCAP.CM.TK_Krankengeschichten.Setup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkSkipValidationIfBatchIsValid;
        private System.Windows.Forms.CheckBox chkSetValidDocumentsValidated;
        private System.Windows.Forms.ComboBox cboBFRXPatientennummer;
        private System.Windows.Forms.ComboBox cboRXBFFallzahl;
        private System.Windows.Forms.ComboBox cboBFDokArtOETranslation;
        private System.Windows.Forms.Label label3;
    }
}