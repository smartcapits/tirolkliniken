﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kofax.Capture.DBLite;
using Kofax.Capture.SDK.CustomModule;
using Kofax.Capture.SDK.Data;

namespace SmartCAP.CM.TK_Krankengeschichten
{
    public static class SmartCMTools
    {
        public static class CSS
        {
            public static class Runtime
            {
                public static string Get(IACDataElement document, string cssName, string cssNotFoundValue = null)
                {
                    try
                    { return document.FindChildElementByName("DocumentCustomStorageStrings").FindChildElementByAttribute("DocumentCustomStorageString", "Name", cssName)["Value"]; }
                    catch (Exception)
                    {
                        if (cssNotFoundValue == null)
                        { throw new Exception("GetCSS - CSS \"" + cssName + "\" does not exist"); }
                        else
                        { return cssNotFoundValue; }
                    }
                }

                public static void Set(IACDataElement document, string cssName, string cssValue)
                {
                    // -- Saves runtime CSS --
                    IACDataElement cssElement = document.FindChildElementByName("DocumentCustomStorageStrings");

                    try
                    {
                        try
                        {
                            // If CSS exists, set it. Otherwise an error will be fired
                            cssElement.FindChildElementByAttribute("DocumentCustomStorageString", "Name", cssName)["Value"] = cssValue;
                        }
                        catch (Exception)
                        {
                            // CSS does not exist -> create it, set value
                            IACDataElement newCSS = cssElement.CreateChildElement("DocumentCustomStorageString");
                            newCSS["Name"] = cssName;
                            newCSS["Value"] = cssValue;
                        }
                    }
                    catch (Exception ex)
                    { throw new Exception("Error in SetCSS! Msg: " + ex.Message); }
                }
            }

            public static class Setup
            {
                public static string Get(IACDataElement runtime, IACDataElement setup, string cssName, string cssNotFoundValue = null)
                {
                    /// <summary>
                    /// Get a specific custom storage strings that was set during setup in administration
                    /// </summary>
                    /// <param name="runtime"></param>
                    /// <param name="setup"></param>
                    /// <param name="cssName"></param>
                    /// <param name="cssNotFoundValue"></param>
                    /// <returns>string content of CSS</returns>
                    try
                    {
                        IACDataElement batchClassElementsCollection = setup.FindChildElementByName("BatchClasses").FindChildElementByAttribute("BatchClass", "Name", runtime.FindChildElementByName("Batch")["BatchClassName"]);
                        IACDataElement cssRootElement = batchClassElementsCollection.FindChildElementByName("BatchClassCustomStorageStrings");
                        // return CSS value
                        return cssRootElement.FindChildElementByAttribute("BatchClassCustomStorageString", "Name", cssName)["Value"];
                    }
                    catch (Exception)
                    {
                        if (cssNotFoundValue == null)
                        { throw new Exception("GetSetupCSS - SetupCustomStorageString \"" + cssName + "\" does not exist"); }
                        else
                        { return cssNotFoundValue; };
                    }
                }
            }
        }


        public static class Document
        {
            public static class IndexField
            {
                public static string Get(IACDataElement document, string fieldName, string fieldNotFoundValue = null)
                {
                    try
                    { return document.FindChildElementByName("IndexFields").FindChildElementByAttribute("IndexField", "Name", fieldName)["Value"]; }
                    catch (Exception)
                    {
                        if (fieldNotFoundValue == null)
                        { throw new Exception("GetIndexField - Indexfeld \"" + fieldName + "\" does not exist"); }
                        else
                        { return fieldNotFoundValue; }
                    }
                }

                public static bool Set(IACDataElement document, string fieldName, string fieldValue)
                {
                    // -- Speichert ein Indexfeld ab (setzt Wert wenn Feld vorhanden ist) --

                    try
                    { document.FindChildElementByName("IndexFields").FindChildElementByAttribute("IndexField", "Name", fieldName)["Value"] = fieldValue; }
                    catch (Exception)
                    { return false; }

                    return true;
                }

                public static string GetDefaultValue(IBatch batch, string formType, string fieldName, string fieldNotFoundValue = null)
                {
                    try
                    {
                        IACDataElement setup = batch.ExtractSetupACDataElement(0);
                        IACDataElement documentClass = setup.FindChildElementByName("DocumentClasses").FindChildElementByAttribute("DocumentClass", "Name", GetClassName(batch, formType));
                        string indexFieldTypeName = documentClass.FindChildElementByName("IndexFieldDefinitions").FindChildElementByAttribute("IndexFieldDefinition", "Name", fieldName)["FieldTypeName"];

                        return setup.FindChildElementByName("FieldTypes").FindChildElementByAttribute("FieldType", "Name", indexFieldTypeName)["DefaultValMacro"];
                    }
                    catch (Exception e)
                    {
                        if (fieldNotFoundValue == null) { throw new Exception("Error in Batch-BatchField.GetDefaultValue - Msg: " + e.Message); }
                        else { return fieldNotFoundValue; }
                    }
                }

                public static List<string> GetSuggestedValues(IBatch batch, string docClass, string fieldName)
                {
                    List<string> suggestedValues = new List<string>();

                    IACDataElement setup = batch.ExtractSetupACDataElement(0);
                    IACDataElement documentClass = setup.FindChildElementByName("DocumentClasses").FindChildElementByAttribute("DocumentClass", "Name", docClass);

                    string indexFieldTypeName = documentClass.FindChildElementByName("IndexFieldDefinitions").FindChildElementByAttribute("IndexFieldDefinition", "Name", fieldName)["FieldTypeName"];
                    IACDataElement fieldType = setup.FindChildElementByName("FieldTypes").FindChildElementByAttribute("FieldType", "Name", indexFieldTypeName);

                    foreach (IACDataElement sV in fieldType.FindChildElementByName("Values").FindChildElementsByName("Value")) { suggestedValues.Add(sV["Text"]); }

                    return suggestedValues;
                }
            }

            public static string GetClassName(IBatch batch, string formTypeName, string classNotFoundValue = null)
            {
                IACDataElement setup = batch.ExtractSetupACDataElement(0);

                foreach (IACDataElement dC in setup.FindChildElementByName("DocumentClasses").FindChildElementsByName("DocumentClass"))
                {
                    foreach (IACDataElement fT in dC.FindChildElementByName("FormTypes").FindChildElementsByName("FormType")) { if (fT["Name"] == formTypeName) { return dC["Name"]; } }
                }

                if (classNotFoundValue == null) { throw new Exception("Error in Batch.GetClassName - Msg: FormType '" + formTypeName + "' not found"); }
                else { return classNotFoundValue; };
            }

            public static void Reject(IACDataElement document, string errorMessage)
            {
                document["Rejected"] = "1";
                document["Note"] = errorMessage;
            }

            public static void UnReject(IACDataElement document)
            {
                document["Rejected"] = "0";
                document["Note"] = "";
            }

            public static void SetIndexed(IACDataElement document)
            {
                document["AutoIndexed"] = "1";
                document["FormsProcessingComplete"] = "1";
            }

            public static void SetUnIndexed(IACDataElement document)
            {
                document["AutoIndexed"] = "0";
                document["FormsProcessingComplete"] = "0";
            }

            public static void SetValidated(IACDataElement document)
            {
                document["Indexed"] = "1";
                document["IndexVerified"] = "1";
                document["Note"] = "";
            }

            public static void SetUnValidated(IACDataElement document, string errorMessage)
            {
                document["Indexed"] = "0";
                document["IndexVerified"] = "0";
                document["Note"] = errorMessage;
            }

            public static bool DeletePage(ref IACDataElement document, int pageNumber, int minPagesBeforeDelete = 2)
            {
                // -- Löscht die angegebene Seite im Dokument --
                int currentPageNumber = 1;
                IACDataElementCollection pagesCollection = document.FindChildElementByName("Pages").FindChildElementsByName("Page");

                // Nur wenn wir genug Pages haben
                if (pagesCollection.Count >= minPagesBeforeDelete)
                {
                    // Checken dass zu löschende PageNumber auch existiert
                    if (pageNumber <= pagesCollection.Count)
                    {
                        foreach (IACDataElement page in pagesCollection)
                        {
                            if (currentPageNumber == pageNumber)
                            {
                                // Seite löschen, True zurückgeben
                                page.Delete();
                                return true;
                            }
                            else
                            { currentPageNumber++; }
                        }
                    }
                }

                return false;
            }

            public static void CopyIndexFields(IACDataElement sourceDoc, IACDataElement targetDoc, bool overwriteNonEmptyFields = false)
            {
                foreach (IACDataElement iF in sourceDoc.FindChildElementByName("IndexFields").FindChildElementsByName("IndexField"))
                {
                    if (overwriteNonEmptyFields)
                    { IndexField.Set(targetDoc, iF["Name"], iF["Value"]); }
                    else
                    {
                        try
                        { if (IndexField.Get(targetDoc, iF["Name"]) == "") { IndexField.Set(targetDoc, iF["Name"], iF["Value"]); } }
                        catch (Exception)
                        { } // Do nothing, Field does not exist on target doc
                    }
                }
            }

            public static void MoveToBatch(IACDataElement document, IBatch targetBatch)
            {
                try
                {
                    IACDataElement runtimeElement = targetBatch.ExtractRuntimeACDataElement(0);
                    IACDataElement batchRootElement = runtimeElement.FindChildElementByName("Batch");
                    IACDataElement targetBatchDocumentsElement = batchRootElement.FindChildElementByName("Documents");

                    targetBatchDocumentsElement.MoveElementToBatch(ref document);
                }
                catch (Exception ex)
                { throw new Exception("Error in MoveDocumentToBatch! Msg: " + ex.Message); }
            }

            public static void MoveToBatchDocument(IACDataElement targetBatch, IACDataElement sourceDoc, IACDataElement targetDoc)
            {
                try
                {
                    while (sourceDoc.FindChildElementByName("Pages").FindChildElementsByName("Page").Count > 0)
                    {
                        // Move Page to targetBatch
                        IACDataElement nPage = sourceDoc.FindChildElementByName("Pages").FindChildElementsByName("Page")[1];
                        IACDataElement nBatchPage = targetBatch.FindChildElementByName("Pages").MoveElementToBatch(ref nPage);
                        // Move new page to document in new batch
                        IACDataElement nDocPages = targetDoc.FindChildElementByName("Pages");
                        nBatchPage.MoveToBack(ref nDocPages);
                    }
                }
                catch (Exception ex)
                { throw new Exception("Error in MoveDocumentToBatchDocument! Msg: " + ex.Message); }
            }
        }


        public static class Batch
        {
            public static class BatchField
            {
                public static string Get(IBatch batch, string fieldName, string fieldNotFoundValue = null)
                {
                    try
                    { return batch.ExtractRuntimeACDataElement(0).FindChildElementByName("Batch").FindChildElementByName("BatchFields").FindChildElementByAttribute("BatchField", "Name", fieldName)["Value"]; }
                    catch (Exception)
                    {
                        if (fieldNotFoundValue == null)
                        { throw new Exception("GetBatchField - BatchField \"" + fieldName + "\" existiert nicht"); }
                        else
                        { return fieldNotFoundValue; }
                    }
                }

                public static bool Set(IBatch batch, string fieldName, string fieldValue)
                {
                    try
                    { batch.ExtractRuntimeACDataElement(0).FindChildElementByName("Batch").FindChildElementByName("BatchFields").FindChildElementByAttribute("BatchField", "Name", fieldName)["Value"] = fieldValue; }
                    catch (Exception)
                    { return false; }

                    return true;
                }

                public static string GetDefaultValue(IBatch batch, string fieldName, string fieldNotFoundValue = null)
                {
                    try
                    {
                        IACDataElement runtime = batch.ExtractRuntimeACDataElement(0);
                        IACDataElement setup = batch.ExtractSetupACDataElement(0);
                        IACDataElement batchClass = setup.FindChildElementByName("BatchClasses").FindChildElementByAttribute("BatchClass", "Name", runtime.FindChildElementByName("Batch")["BatchClassName"]);

                        return batchClass.FindChildElementByName("BatchFieldDefinitions").FindChildElementByAttribute("BatchFieldDefinition", "Name", fieldName)["DefaultValMacro"];
                    }
                    catch (Exception e)
                    {
                        if (fieldNotFoundValue == null) { throw new Exception("Error in Batch-BatchField.GetDefaultValue - Msg: " + e.Message); }
                        else { return fieldNotFoundValue; }
                    }
                }

                public static List<string> GetSuggestedValues(IBatch batch, string fieldName)
                {
                    List<string> suggestedValues = new List<string>();

                    IACDataElement runtime = batch.ExtractRuntimeACDataElement(0);
                    IACDataElement setup = batch.ExtractSetupACDataElement(0);
                    IACDataElement batchClass = setup.FindChildElementByName("BatchClasses").FindChildElementByAttribute("BatchClass", "Name", runtime.FindChildElementByName("Batch")["BatchClassName"]);

                    string batchFieldTypeName = batchClass.FindChildElementByName("BatchFieldDefinitions").FindChildElementByAttribute("BatchFieldDefinition", "Name", fieldName)["FieldTypeName"];
                    IACDataElement bfDefinition = setup.FindChildElementByName("FieldTypes").FindChildElementByAttribute("FieldType", "Name", batchFieldTypeName);

                    foreach (IACDataElement sV in bfDefinition.FindChildElementByName("Values").FindChildElementsByName("Value")) { suggestedValues.Add(sV["Text"]); }

                    return suggestedValues;
                }
            }

            public static int DeleteEmptyDocuments(IACDataElement batch)
            {
                int deletedDocs = 0;

                for (int currentDoc = batch.FindChildElementByName("Documents").FindChildElementsByName("Document").Count; currentDoc > 0; currentDoc--)
                {
                    if (batch.FindChildElementByName("Documents").FindChildElementsByName("Document")[currentDoc].FindChildElementByName("Pages").FindChildElementsByName("Page").Count == 0)
                    {
                        batch.FindChildElementByName("Documents").FindChildElementsByName("Document")[currentDoc].Delete();
                        deletedDocs++;
                    }
                }

                return deletedDocs;
            }

            public static void CopyBatchFields(IBatch sourceBatch, IBatch targetBatch, bool overwriteNonEmptyFields = false)
            {
                foreach (IACDataElement bF in sourceBatch.ExtractRuntimeACDataElement(0).FindChildElementByName("Batch").FindChildElementByName("BatchFields").FindChildElementsByName("BatchField"))
                {
                    if (overwriteNonEmptyFields)
                    { BatchField.Set(targetBatch, bF["Name"], bF["Value"]); }
                    else
                    {
                        try
                        {
                            string bfDefaultValue = BatchField.GetDefaultValue(targetBatch, bF["Name"], "");
                            if (bfDefaultValue == "") { BatchField.Set(targetBatch, bF["Name"], bF["Value"]); }     // Check if the batchfield has a default value (which is not automatically set upon batch creation). 
                            else { BatchField.Set(targetBatch, bF["Name"], bfDefaultValue); }
                        }
                        catch (Exception)
                        { } // Do nothing, Field does not exist on target batch
                    }
                }
            }
        }


        public static class LossePage
        {
            public static void MoveToBatch(IACDataElement targetBatch, IACDataElement sourceLoosePage)
            {
                try
                {
                    targetBatch.FindChildElementByName("Pages").MoveElementToBatch(ref sourceLoosePage);
                }
                catch (Exception ex)
                { throw new Exception("Error in MoveLoosePageToBatch! Msg: " + ex.Message); }
            }
        }


        public static class Workflow
        {
            public static string GetModuleID(IBatch batch, string moduleName)
            {
                IACDataElement setup = batch.ExtractSetupACDataElement(0);

                foreach (IACDataElement process in setup.FindChildElementByName("Processes").FindChildElementsByName("Process"))
                {
                    if (process["Name"] == moduleName) { return process["ModuleID"]; }
                }

                throw new Exception("GetModuleID - Cannot find module '" + moduleName + "'");
            }


            public static bool ModuleExists(IBatch batch, string moduleEXEName)
            {
                IACDataElement runtimeElement = batch.ExtractRuntimeACDataElement(0);
                IACDataElement batchRootElement = runtimeElement.FindChildElementByName("Batch");

                try
                {
                    foreach (IACDataElement module in batchRootElement.FindChildElementByName("BatchModules").FindChildElementsByName("BatchModule"))
                    { if (module["ModuleID"] == moduleEXEName) { return true; } }
                }
                catch (Exception ex)
                { throw new Exception("Error in ModuleExists! Msg: " + ex.Message); }

                // Modul nicht vorhanden
                return false;
            }

            public static string GetModuleAfterModule(IBatch batch, string baseModuleID)
            {
                // -- Returns the next module after a given (base) module --
                bool trigger = false;

                IACDataElement runtimeElement = batch.ExtractRuntimeACDataElement(0);
                IACDataElement batchRootElement = runtimeElement.FindChildElementByName("Batch");
                IACDataElement modulesElement = batchRootElement.FindChildElementByName("BatchModules");
                IACDataElementCollection modulesCollection = modulesElement.FindChildElementsByName("BatchModule");

                try
                {
                    foreach (IACDataElement module in modulesCollection)
                    {
                        if (trigger) { return module["ModuleID"]; }

                        if (module["ModuleID"] == baseModuleID) { trigger = true; }
                    }
                }
                catch (Exception ex)
                { throw new Exception("Error in GetModuleAfterModule! Msg: " + ex.Message); }

                // return base module if no next module has been found (likely base module is last module in queue)
                return baseModuleID;
            }

            public static KfxDbState GetBatchState(string stateName)
            {
                switch (stateName.ToUpper())
                {
                    case "READY": return KfxDbState.KfxDbBatchReady;
                    case "SUSPENDED": return KfxDbState.KfxDbBatchSuspended;
                    case "RESERVED": return KfxDbState.KfxDbBatchReserved;
                    case "ERROR": return KfxDbState.KfxDbBatchError;
                    case "COMPLETED": return KfxDbState.KfxDbBatchCompleted;
                    default: return KfxDbState.KfxDbBatchReady;
                }
            }
        }

        
        public static class Filter
        {
            public static string Out(string inputString, string forbiddenChars)
            {
                // Filtert unerwünschte Zeichen aus einem String heraus

                string returnValue = "";

                try
                {
                    for (int i = 0; i < inputString.Length; i++)
                    {
                        if (!forbiddenChars.Contains(inputString.Substring(i, 1)))
                        {
                            returnValue += inputString.Substring(i, 1);
                        }
                    }
                }
                catch (Exception ex)
                { throw new Exception("Error in FilterOut! Msg: " + ex.Message); }

                return returnValue;
            }

            public static string Positive(string inputString, string allowedChars)
            {
                // Lässt nur erwünschte Zeichen zu

                string returnValue = "";

                try
                {
                    for (int i = 0; i < inputString.Length; i++)
                    {
                        if (allowedChars.Contains(inputString.Substring(i, 1)))
                        {
                            returnValue += inputString.Substring(i, 1);
                        }
                    }
                }
                catch (Exception ex)
                { throw new Exception("Error in FilterPositive! Msg: " + ex.Message); }

                return returnValue;
            }
        }


        public static class Validators
        {
    //        Public Function CheckNumericField(ByRef fieldToCheck As DRID.Field, ByVal mandatoryField As Boolean, Optional ByVal minNumber As Double = Nothing, Optional ByVal maxNumber As Double = Nothing, Optional ByVal numberOfDigits As Integer = Nothing, Optional ByVal numbersAllowed As String = "0123456789", Optional ByVal errorMessage As String = "") As Boolean

    //    Dim returnValue As Boolean = True
    //    Dim number As Double
    //    Dim errorDescription As String = ""

    //    Try
    //        'Auf Mandatory Field checken
    //        If mandatoryField Then
    //            If fieldToCheck.Value.Text.Length.Equals(0) Then
    //                returnValue = False
    //                errorDescription = "Dieses Feld muss einen Wert beinhalten"
    //            End If
    //        End If

    //        'Auf erlaubte Nummern checken
    //        If returnValue Then
    //            If Not fieldToCheck.Value.Text.Equals(FilterPositive(fieldToCheck.Value.Text, numbersAllowed)) Then
    //                returnValue = False
    //                errorDescription = "Der angegebene Wert beinhaltet ungültige Zeichen"
    //            End If
    //        End If

    //        'Auf Anzahl Digits checken
    //        If returnValue Then
    //            If Not numberOfDigits = Nothing And fieldToCheck.Value.Text.Length > 0 And Not fieldToCheck.Value.Text.Length.Equals(numberOfDigits) Then
    //                returnValue = False
    //                errorDescription = "Es wird nur eine Zahl mit '" + numberOfDigits.ToString() + "' Ziffern akzeptiert"
    //            End If
    //        End If

    //        'Auf minimale Zahl checken
    //        If returnValue Then
    //            If Not minNumber = Nothing And fieldToCheck.Value.Text.Length > 0 Then
    //                number = Double.Parse(fieldToCheck.Value.Text)
    //                If number < minNumber Then
    //                    returnValue = False
    //                    errorDescription = "Der angegebene Wert ist kleiner als der minimal erlaubte Wert '" + minNumber.ToString() + "'"
    //                End If
    //            End If
    //        End If

    //        'Auf maximale Zahl checken
    //        If returnValue Then
    //            If Not maxNumber = Nothing And fieldToCheck.Value.Text.Length > 0 Then
    //                number = Double.Parse(fieldToCheck.Value.Text)
    //                If number > maxNumber Then
    //                    returnValue = False
    //                    errorDescription = "Der angegebene Wert ist größer als der maximal erlaubte Wert '" + maxNumber.ToString() + "'"
    //                End If
    //            End If
    //        End If

    //    Catch ex As Exception
    //        'Throw New Exception("Error in CheckNumberRange! Msg: " + ex.Message)
    //        errorDescription = "Allgemeiner Fehler bei der Zahlenprüfung -> " + ex.Message
    //        returnValue = False
    //    End Try

    //    'Checken ob Feld gültig oder ungült ist -> Fehlermeldung setzen oder löschen
    //    If returnValue Then
    //        fieldToCheck.ErrorDescription = ""
    //    Else
    //        If errorMessage<> "" Then
    //            fieldToCheck.ErrorDescription = errorMessage
    //        Else
    //            fieldToCheck.ErrorDescription = errorDescription
    //        End If
    //    End If

    //    Return returnValue

    //End Function

        }
    }

}
