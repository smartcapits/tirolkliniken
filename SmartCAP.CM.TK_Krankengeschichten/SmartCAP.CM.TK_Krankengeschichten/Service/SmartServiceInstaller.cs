﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace SmartCAP.CM.TK_Krankengeschichten
{
    [RunInstaller(true)]
    public partial class SmartServiceInstaller : Installer
    {
        public static string ServiceName { get; set; }
        public static string DisplayName { get; set; }
        public static string Description { get; set; }
        public static Type ServiceType { get; set; }

        public SmartServiceInstaller()
        {
            serviceProcessInstaller1 = new ServiceProcessInstaller();
            serviceInstaller1 = new ServiceInstaller();

            serviceProcessInstaller1.Account = ServiceAccount.LocalSystem;
            serviceInstaller1.StartType = ServiceStartMode.Manual;

            serviceInstaller1.ServiceName = ServiceName;
            serviceInstaller1.DisplayName = DisplayName;
            serviceInstaller1.Description = Description;

            Installers.Add(serviceProcessInstaller1);
            Installers.Add(serviceInstaller1);

        }


        /// <summary>
        /// Installs a service of ServiceType with ServiceName and ServiceDescription, if it is not already installed
        /// </summary>
        /// <param name="multiInstance">true installs up to 32 instances of the service</param>
        public static void InstallService(string[] args, bool multiInstance = false)
        {
            // todo: if multi-instance - check for the next instance to install
            int serviceInstanceCount = GetServiceInstanceCount();
            if (serviceInstanceCount == 0)
            {
                // continue in every case
            }
            else if (multiInstance == false)
            {
                // return, as at least one service is installed and we don't want multiple instances
                return;
            }
            else if (serviceInstanceCount == 32)
            {
                // return, as we do not support more than 32 instances
                return;
            }
            else
            {
                // change the service name to the next instance count
                ServiceName = ServiceName + ":" + (serviceInstanceCount + 1);
                DisplayName = DisplayName + ":" + (serviceInstanceCount + 1);
            }

            //if (IsInstalled()) return;

            try
            {
                using (AssemblyInstaller installer = GetInstaller())
                {
                    IDictionary state = new Hashtable();
                    try
                    {
                        installer.Install(state);
                        installer.Commit(state);

                        // add service arguments, if provided
                        if (args.Length > 1)
                        {
                            string parameters = String.Join(" ", args.Where(a => a != "-install"));
                            string servicePath = @"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\" + ServiceName;
                            string imagePath = Microsoft.Win32.Registry.GetValue(servicePath, "ImagePath", "").ToString();
                            Microsoft.Win32.Registry.SetValue(servicePath, "ImagePath", imagePath + " " + parameters);
                        }
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            installer.Rollback(state);
                        }
                        catch { }
                        throw;
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Uninstalls the newest instance of a service with ServiceName
        /// </summary>
        public static void UninstallService()
        {
            int serviceInstanceCount = GetServiceInstanceCount();
            if (serviceInstanceCount == 0)
            {
                // return, as the is no service installed
                return;
            }
            else if (serviceInstanceCount == 1)
            {
                // continue uninstallation with the original service name
            }
            else
            {
                // continue uninstallation with the newest service instance
                ServiceName = ServiceName + ":" + serviceInstanceCount;
                DisplayName = DisplayName + ":" + serviceInstanceCount;
            }

            //if (!IsInstalled()) return;

            try
            {
                using (AssemblyInstaller installer = GetInstaller())
                {
                    IDictionary state = new Hashtable();
                    try
                    {
                        installer.Uninstall(state);
                    }
                    catch
                    {
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static void StartService()
        {
            if (!IsInstalled()) return;

            using (ServiceController controller =
                new ServiceController(ServiceName))
            {
                try
                {
                    if (controller.Status != ServiceControllerStatus.Running)
                    {
                        controller.Start();
                        controller.WaitForStatus(ServiceControllerStatus.Running,
                            TimeSpan.FromSeconds(10));
                    }
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }

        public static void StopService()
        {
            if (!IsInstalled()) return;
            using (ServiceController controller =
                new ServiceController(ServiceName))
            {
                try
                {
                    if (controller.Status != ServiceControllerStatus.Stopped)
                    {
                        controller.Stop();
                        controller.WaitForStatus(ServiceControllerStatus.Stopped,
                             TimeSpan.FromSeconds(10));
                    }
                }
                catch
                {
                    throw;
                }
            }
        }

        private static bool IsInstalled()
        {
            using (ServiceController controller =
                new ServiceController(ServiceName))
            {
                try
                {
                    ServiceControllerStatus status = controller.Status;
                }
                catch
                {
                    return false;
                }
                return true;
            }
        }

        private static int GetServiceInstanceCount()
        {
            return ServiceController.GetServices().Count(s => s.ServiceName.StartsWith(ServiceName));
        }

        private static bool IsRunning()
        {
            using (ServiceController controller =
                new ServiceController(ServiceName))
            {
                if (!IsInstalled()) return false;
                return (controller.Status == ServiceControllerStatus.Running);
            }
        }

        private static AssemblyInstaller GetInstaller()
        {
            AssemblyInstaller installer = new AssemblyInstaller(
                ServiceType.Assembly, null);
            //installer.UseNewContext = true;
            return installer;
        }
    }
}
