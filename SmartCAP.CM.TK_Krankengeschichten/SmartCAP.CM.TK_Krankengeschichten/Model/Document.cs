﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using log4net;
using System.Reflection;
using Kofax.Capture.SDK.Data;

namespace SmartCAP.CM.Model
{
    public class Document
    {
        /// <summary>
        /// A list of all BatchFields
        /// </summary>
        public Dictionary<string, string> BatchFields { get; private set; }
        /// <summary>
        /// A list of all IndexFields
        /// </summary>
        public Dictionary<string, string> IndexFields { get; private set; }
        /// <summary>
        /// A list of all TextConstants
        /// </summary>
        public Dictionary<string, string> TextConstants { get; set; }
        /// <summary>
        /// A list of all CustomProperties (saved settings during setup)
        /// </summary>        
        public Dictionary<string, string> CustomProperties { get; private set; }

        /// <summary>
        /// Contains a list of the document source files (e.g. single page tifs or pdf, if e-documents were imported)
        /// </summary>
        public List<string> ImageFileNames { get; private set; }
        /// <summary>
        /// Contains the path to the XDocument, if KTM Server was executed
        /// </summary>
        public string XdcFilePath { get; private set; }

        /// <summary>
        /// Contains the document as multipage-tif, if the source files were tif-files (will be null in case of source pdf / e-documents)
        /// </summary>
        public List<byte[]> BinaryImages { get; private set; }

        public List<Page> Pages { get; private set; }

        private static readonly ILog Log =
           LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.BaseType.Name);

        /// <summary>
        /// initializes the document from a releasedata object, filling index and batch fields, as well as text constants and more stuff.
        /// </summary>
        /// <param name="DocumentData"></param>
        public Document(IACDataElement DocumentData)
        {
            BatchFields = new Dictionary<string, string>();
            IndexFields = new Dictionary<string, string>();
            TextConstants = new Dictionary<string, string>();
            CustomProperties = new Dictionary<string, string>();
            ImageFileNames = new List<string>();
            BinaryImages = new List<byte[]>();
            Pages = new List<Page>();


            // add index and batch fields
            Log.Debug("add index and batch fields");
            IACDataElementCollection indexFields = DocumentData.FindChildElementByName("IndexFields").FindChildElementsByName("IndexField");

            foreach (IACDataElement indexField in indexFields)
            {
                IndexFields.Add(indexField["Name"], indexField["Value"]);
            }

            //add pages
            Log.Debug("add pages");
            IACDataElementCollection pages = DocumentData.FindChildElementByName("Pages").FindChildElementsByName("Page");

            int pageNumber = 1;
            foreach (IACDataElement page in pages)
            {
                ImageFileNames.Add(page["ImageFile"]);
                BinaryImages.Add(File.ReadAllBytes(page["ImageFile"]));

                Pages.Add(new Page() { pageNumber = pageNumber, filePath = page["ImageFile"] });
                pageNumber++;
            }
        }

        public string GetIndexField(string key, string valueIfNotFound)
        {
            string k = key.Length > 32 ? key.Substring(0, 32) : key;
            if (IndexFields.ContainsKey(k)) return IndexFields[k];
            else return valueIfNotFound;
        }
    }
}
