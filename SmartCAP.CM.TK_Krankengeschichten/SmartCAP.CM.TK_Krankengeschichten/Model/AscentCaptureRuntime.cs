﻿using Kofax.Capture.SDK.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCAP.CM.TK_Krankengeschichten.Model
{
    /// <summary>
    /// This is the root element for the Kofax Capture runtime data. 
    /// </summary>
    class AscentCaptureRuntime
    {
        private IACDataElement runtime;
        private Batch batch;
        private AutoNumber autoNumber;
        public AscentCaptureRuntime(IACDataElement runtime)
        {
            this.runtime = runtime;
            batch = new Batch(runtime.FindChildElementByName("Batch"));
            autoNumber = new AutoNumber(runtime.FindChildElementByName("AutoNumber"));
        }

        public int DataBaseVersion
        {
            get
            {
                return Convert.ToInt32(runtime["DataBaseVersion"]);
            }
            set
            {
                runtime["DataBaseVersion"] = value.ToString();
            }
        }
        /// <summary>
        /// Export format version
        /// </summary>
        public int DataDefinitionVersion
        {
            get
            {
                return Convert.ToInt32(runtime["DataDefinitionVersion"]);
            }
            set
            {
                runtime["DataDefinitionVersion"] = value.ToString();
            }
        }

        public string xmlns
        {
            get
            {
                return runtime["xmlns"];
            }
            set
            {
                runtime["DataDefinitionVersion"] = value;
            }
        }
        /// <summary>
        /// This defines a batch. 
        /// </summary>
        public Batch Batch
        {
            get
            {
                return batch;
            }
        }

        public AutoNumber AutoNumber
        {
            get
            {
                return autoNumber;
            }
        }
    }

    class Batch
    {
        private IACDataElement batch;
        private BatchFields batchFields;
        private BatchField[] batchFields2;
        public Batch(IACDataElement batch)
        {
            this.batch = batch;
            batchFields = new BatchFields(batch.FindChildElementByName("BatchFields"));


            IACDataElementCollection batchFieldCollection = batch.FindChildElementByName("BatchFields").FindChildElementsByName("BatchField");
            this.batchFields2 = new BatchField[batchFieldCollection.Count];
            for (int i = 0; i < batchFieldCollection.Count; i++)
            {
                this.batchFields2[i] = new BatchField(batchFieldCollection[i + 1]); // collection index is one based
            }
        }
        public string Name
        {
            get
            {
                return batch["Name"];
            }
            set
            {
                batch["Name"] = value;
            }
        }
        /// <summary>
        /// Returns the batch ID that is used outside the database to reference the batch.
        /// </summary>
        public int ExternalBatchID
        {
            get
            {
                return Convert.ToInt32(batch["ExternalBatchID"]);
            }
            set
            {
                batch["ExternalBatchID"] = value.ToString();
            }
        }
        /// <summary>
        /// The value of this attribute corresponds to the "Name" of an element of type "BatchClass." 
        /// </summary>
        public string BatchClassName
        {
            get
            {
                return batch["BatchClassName"];
            }
            set
            {
                batch["BatchClassName"] = value;
            }
        }
        /// <summary>
        /// Internal use only.
        /// </summary>
        public int LogicalBatchID
        {
            get
            {
                return Convert.ToInt32(batch["LogicalBatchID"]);
            }
            set
            {
                batch["LogicalBatchID"] = value.ToString();
            }
        }
        /// <summary>
        ///  	The process the batch is currently in. 
        ///  	The value of this attribute corresponds to the "ModuleID" of an element of type "Module." 
        /// </summary>
        public string ModuleID
        {
            get
            {
                return batch["ModuleID"];
            }
            set
            {
                batch["ModuleID"] = value;
            }
        }
        /// <summary>
        /// Collection of "BatchField" elements.
        /// </summary>
        public BatchFields BatchFields
        {
            get
            {
                return batchFields;
            }
        }
        public BatchField[] BatchFields2 // or better like this?
        {
            get
            {
                return batchFields2;
            }
        }
    }

    class BatchFields
    {
        private BatchField[] batchFields;
        public BatchFields(IACDataElement batchFields)
        {
            IACDataElementCollection batchFieldCollection = batchFields.FindChildElementsByName("BatchField");
            this.batchFields = new BatchField[batchFieldCollection.Count];
            for (int i = 0; i < batchFieldCollection.Count; i++)
            {
                this.batchFields[i] = new BatchField(batchFieldCollection[i + 1]); // collection index is one based
            }
        }
        /// <summary>
        /// This defines a batch field value. 
        /// </summary>
        public BatchField[] BatchField
        {
            get
            {
                return batchFields;
            }
        }
    }

    class BatchField
    {
        private IACDataElement batchField;
        public BatchField(IACDataElement batchField)
        {
            this.batchField = batchField;
        }
        /// <summary>
        /// The value of this attribute corresponds to the "Name" of an element of type "BatchFieldDefinition." 
        /// </summary>
        public string Name
        {
            get
            {
                return batchField["Name"];
            }
            set
            {
                batchField["Name"] = value;
            }
        }
        public string Value
        {
            get
            {
                return batchField["Value"];
            }
            set
            {
                batchField["Value"] = value;
            }
        }
        /// <summary>
        /// Batch Field Valid.  
        /// </summary>
        public bool Valid
        {
            get
            {
                return Convert.ToBoolean(batchField["Valid"]);
            }
            set
            {
                batchField["Valid"] = value.ToString();
            }
        }
        /// <summary>
        /// Batch Field Valid Attribute Changed.  
        /// </summary>
        public bool ValidChanged
        {
            get
            {
                return Convert.ToBoolean(batchField["ValidChanged"]);
            }
            set
            {
                batchField["ValidChanged"] = value.ToString();
            }
        }
        /// <summary>
        /// Batch Field Value Changed. 
        /// </summary>
        public bool ValueChanged
        {
            get
            {
                return Convert.ToBoolean(batchField["ValueChanged"]);
            }
            set
            {
                batchField["ValueChanged"] = value.ToString();
            }
        }
        /// <summary>
        /// Indicate if a batch field is changed by an interactive operation. The custom module or WFA can set or get value for the reporting purpose
        /// </summary>
        public bool UserChanged
        {
            get
            {
                return Convert.ToBoolean(batchField["UserChanged"]);
            }
            set
            {
                batchField["UserChanged"] = value.ToString();
            }
        }
    }

    class AutoNumber
    {
        private IACDataElement autoNumber;
        public AutoNumber(IACDataElement autoNumber)
        {
            this.autoNumber = autoNumber;
        }
        public int Type
        {
            get
            {
                return Convert.ToInt32(autoNumber["Type"]);
            }
            set
            {
                autoNumber["Type"] = value.ToString();
            }
        }
        public int BatchID
        {
            get
            {
                return Convert.ToInt32(autoNumber["BatchID"]);
            }
            set
            {
                autoNumber["BatchID"] = value.ToString();
            }
        }
        public string ColumnName
        {
            get
            {
                return autoNumber["Type"];
            }
            set
            {
                autoNumber["Type"] = value;
            }
        }
        public int NextID
        {
            get
            {
                return Convert.ToInt32(autoNumber["NextID"]);
            }
            set
            {
                autoNumber["NextID"] = value.ToString();
            }
        }
    }
}
