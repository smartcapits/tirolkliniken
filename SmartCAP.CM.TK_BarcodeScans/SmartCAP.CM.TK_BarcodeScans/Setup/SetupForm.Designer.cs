﻿namespace SmartCAP.CM.TK_BarcodeScans
{
    partial class SetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupForm));
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cboBFDokArtBCTranslation = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkSetValidDocumentsValidated = new System.Windows.Forms.CheckBox();
            this.chkSkipValidationIfBatchIsValid = new System.Windows.Forms.CheckBox();
            this.cboDocTypeSource = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboDocTypeTarget = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboBarcodeTarget = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(228, 244);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(147, 53);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(381, 244);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(107, 53);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cboBFDokArtBCTranslation
            // 
            this.cboBFDokArtBCTranslation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboBFDokArtBCTranslation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBFDokArtBCTranslation.FormattingEnabled = true;
            this.cboBFDokArtBCTranslation.Location = new System.Drawing.Point(198, 90);
            this.cboBFDokArtBCTranslation.Name = "cboBFDokArtBCTranslation";
            this.cboBFDokArtBCTranslation.Size = new System.Drawing.Size(291, 24);
            this.cboBFDokArtBCTranslation.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(176, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "DokTyp Barcode Mapping:";
            // 
            // chkSetValidDocumentsValidated
            // 
            this.chkSetValidDocumentsValidated.AutoSize = true;
            this.chkSetValidDocumentsValidated.Checked = true;
            this.chkSetValidDocumentsValidated.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSetValidDocumentsValidated.Location = new System.Drawing.Point(15, 167);
            this.chkSetValidDocumentsValidated.Name = "chkSetValidDocumentsValidated";
            this.chkSetValidDocumentsValidated.Size = new System.Drawing.Size(271, 21);
            this.chkSetValidDocumentsValidated.TabIndex = 13;
            this.chkSetValidDocumentsValidated.Text = "Setzte gültige Dokumente auf Validiert";
            this.chkSetValidDocumentsValidated.UseVisualStyleBackColor = true;
            // 
            // chkSkipValidationIfBatchIsValid
            // 
            this.chkSkipValidationIfBatchIsValid.AutoSize = true;
            this.chkSkipValidationIfBatchIsValid.Location = new System.Drawing.Point(15, 194);
            this.chkSkipValidationIfBatchIsValid.Name = "chkSkipValidationIfBatchIsValid";
            this.chkSkipValidationIfBatchIsValid.Size = new System.Drawing.Size(452, 21);
            this.chkSkipValidationIfBatchIsValid.TabIndex = 12;
            this.chkSkipValidationIfBatchIsValid.Text = "Überspringe Validierung wenn alle Dokumente im Stapel gültig sind";
            this.chkSkipValidationIfBatchIsValid.UseVisualStyleBackColor = true;
            // 
            // cboDocTypeSource
            // 
            this.cboDocTypeSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboDocTypeSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDocTypeSource.FormattingEnabled = true;
            this.cboDocTypeSource.Location = new System.Drawing.Point(198, 17);
            this.cboDocTypeSource.Name = "cboDocTypeSource";
            this.cboDocTypeSource.Size = new System.Drawing.Size(291, 24);
            this.cboDocTypeSource.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Dokumententyp Quelle:";
            // 
            // cboDocTypeTarget
            // 
            this.cboDocTypeTarget.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboDocTypeTarget.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDocTypeTarget.FormattingEnabled = true;
            this.cboDocTypeTarget.Location = new System.Drawing.Point(198, 54);
            this.cboDocTypeTarget.Name = "cboDocTypeTarget";
            this.cboDocTypeTarget.Size = new System.Drawing.Size(291, 24);
            this.cboDocTypeTarget.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 17);
            this.label2.TabIndex = 18;
            this.label2.Text = "Dokumententyp Ziel:";
            // 
            // cboBarcodeTarget
            // 
            this.cboBarcodeTarget.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboBarcodeTarget.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBarcodeTarget.FormattingEnabled = true;
            this.cboBarcodeTarget.Location = new System.Drawing.Point(198, 128);
            this.cboBarcodeTarget.Name = "cboBarcodeTarget";
            this.cboBarcodeTarget.Size = new System.Drawing.Size(291, 24);
            this.cboBarcodeTarget.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 17);
            this.label4.TabIndex = 20;
            this.label4.Text = "Barcode Ziel:";
            // 
            // SetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 309);
            this.Controls.Add(this.cboBarcodeTarget);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cboDocTypeTarget);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboDocTypeSource);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboBFDokArtBCTranslation);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.chkSetValidDocumentsValidated);
            this.Controls.Add(this.chkSkipValidationIfBatchIsValid);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SetupForm";
            this.Text = "SmartCAP.CM.TK_BarcodeScans.Setup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cboBFDokArtBCTranslation;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkSetValidDocumentsValidated;
        private System.Windows.Forms.CheckBox chkSkipValidationIfBatchIsValid;
        private System.Windows.Forms.ComboBox cboDocTypeSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboDocTypeTarget;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboBarcodeTarget;
        private System.Windows.Forms.Label label4;
    }
}