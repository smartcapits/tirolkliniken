﻿//using Kofax.Capture.DBLite;
using Kofax.Capture.SDK.CustomModule;
using Kofax.Capture.SDK.Data;
using log4net;
using Microsoft.Win32;
using SmartCAP.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCAP.CM.TK_BarcodeScans
{
    class Program
    {
        #region log4net
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private void InitializeLogging()
        {
            try
            {
                LoggerFunctions.Initialize(this.GetType().Namespace);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not initialize logging: " + ex.Message);
            }
        }
        #endregion

        //[MethodImpl(MethodImplOptions.NoInlining)]
        static void Run(string[] args)
        {
            if (args.Contains("-install"))
            {
                // install service
                try
                {
                    SmartServiceInstaller.ServiceType = typeof(CustomModuleService);
                    SmartServiceInstaller.ServiceName = CustomModule.ServiceName;
                    SmartServiceInstaller.DisplayName = CustomModule.DisplayName;
                    SmartServiceInstaller.Description = CustomModule.Description;
                    SmartServiceInstaller.InstallService(args, CustomModule.AllowMultiInstance);
                    MessageBox.Show("Service installed successfully");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not install service: " + ex.Message);
                }
            }
            else if (args.Contains("-uninstall"))
            {
                try
                {
                    // uninstall service
                    SmartServiceInstaller.ServiceType = typeof(CustomModuleService);
                    SmartServiceInstaller.ServiceName = CustomModule.ServiceName;
                    SmartServiceInstaller.StopService();
                    SmartServiceInstaller.UninstallService();
                    MessageBox.Show("Service uninstalled successfully");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not uninstall service: " + ex.Message);
                }
            }
            else if (Environment.UserInteractive)
            {
                // run as module
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new RuntimeForm(args));

            }
            else
            {
                // run as service
                ServiceBase.Run(new CustomModuleService(args));
            }
        }

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);

            // needs to be called in separate function, because assembly resolution would not work otherwise
            Run(args);

            return;
        }
    }
}
