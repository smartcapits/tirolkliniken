﻿using Kofax.Capture.DBLite;
using Kofax.Capture.SDK.CustomModule;
using Kofax.Capture.SDK.Data;
using log4net;
using Microsoft.Win32;
using SmartCAP.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SmartCAP.SmartKCBatch;
using System.Text.RegularExpressions;

namespace SmartCAP.CM.TK_BarcodeScans
{
    #region event handlers
    public class BatchEventArgs : EventArgs
    {
        public IBatch Batch { get; private set; }
        public BatchEventArgs(IBatch batch)
        {
            Batch = batch;
        }
    }
    public class ACDataElementEventArgs : EventArgs
    {
        public IACDataElement Element { get; private set; }

        public ACDataElementEventArgs(IACDataElement element)
        {
            Element = element;
        }
    }
    public class TextEventArgs : EventArgs
    {
        public string Text { get; private set; }

        public TextEventArgs(string text)
        {
            Text = text;
        }
    }

    public delegate void BatchEventHandler(object sender, BatchEventArgs e);
    public delegate void ACDataElementEventHandler(object sender, ACDataElementEventArgs e);
    public delegate void TextEventHandler(object sender, TextEventArgs e);
    #endregion event handlers

    public class CustomModule
    {
        #region log4net
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private void InitializeLogging()
        {
            try
            {
                LoggerFunctions.Initialize(this.GetType().Namespace);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not initialize logging: " + ex.Message);
            }
        }
        #endregion        

        public static bool AllowMultiInstance = true;
        public static string ServiceName = "SmartCAP.CM.TK_BarcodeScans";
        public static string DisplayName = "SmartCAP.CM.TK_BarcodeScans Service";
        public static string Description = "Kofax-Service zur Verarbeitung von Barcode-Dokumenten";
        /// <summary>
        /// This setting is only used, if batch notification is disabled
        /// </summary>
        private static double pollIntervalSeconds = 60;

        #region initialization and login

        private Login login;
        private IRuntimeSession session;
        private IBatch activeBatch;
        private Mutex mutex;
        private KfxDbFilter filter = KfxDbFilter.KfxDbFilterOnProcess | KfxDbFilter.KfxDbFilterOnStates | KfxDbFilter.KfxDbSortOnPriorityDescending;
        private KfxDbState states = KfxDbState.KfxDbBatchReady;
        private System.Timers.Timer pollTimer = new System.Timers.Timer();

        public event BatchEventHandler BatchOpened;
        public event BatchEventHandler BatchClosed;
        public event ACDataElementEventHandler DocumentOpened;
        public event ACDataElementEventHandler DocumentClosed;
        public event TextEventHandler ErrorOccured;

        /// <summary>
        /// Returns the batch notification state (existence of DisableBatchNotification registry key)
        /// </summary>
        public static bool BatchNotificationEnabled { get { return GetBatchNotificationState(); } }
        private static bool GetBatchNotificationState()
        {
            string registryKey;
            if (Environment.Is64BitOperatingSystem)
                registryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0";
            else
                registryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Kofax Image Products\Ascent Capture\3.0";

            string value = Convert.ToString(Registry.GetValue(registryKey, "DisableBatchNotification", ""));

            if (String.IsNullOrEmpty(value))
            {
                // DisableBatchNotification key does not exist - batch notification is enabled
                return true;
            }
            else
            {
                // DisableBatchNotificationKey exists - batch notification is disabled
                return false;

            }
        }

        /// <summary>
        /// Provides all necessary methods for batch processing
        /// </summary>
        public CustomModule()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);
            InitializeLogging();
            mutex = new Mutex();
        }

        /// <summary>
        ///  Login to KC
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        public void Login(string user, string password, bool showDialog = false)
        {
            login = new Login();
            try
            {
                login.EnableSecurityBoost = true;
                login.Login();
                login.ApplicationName = "SmartCAP.CM.TK_BarcodeScans";
                //login.Version = "1.0";            
                login.ValidateUser("SmartCAP.CM.TK_BarcodeScans.exe", showDialog, user, password);

                session = login.RuntimeSession;
            }
            catch (Exception ex)
            {
                string message = "Error logging in to KC: " + ex.ToString();
                Log.Error(message);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
                throw;
            }
        }

        /// <summary>
        /// The CustomModule will be notified from Kofax Capture, if new batches are available. 
        /// </summary>
        public void ListenForNewBatches()
        {
            if (session == null)
                throw new Exception("you need to login to KC first");

            session.BatchAvailable += Session_BatchAvailable;
        }

        /// <summary>
        /// Polls for new batches
        /// </summary>
        /// <param name="interval">poll interval in milliseconds</param>
        public void PollForNewBatches()
        {
            pollTimer.Interval = pollIntervalSeconds * 1000;
            pollTimer.Elapsed += PollTimer_Elapsed;
            pollTimer.Enabled = true;
        }

        #endregion initialization and login

        /// <summary>
        /// Processes the currently active batch.
        /// Here all the custom magic happens.
        /// </summary>
        private void ProcessActiveBatch()
        {
            try
            {
                BatchOpened?.Invoke(this, new BatchEventArgs(activeBatch));

                SmartCMBatch smartBatch = new SmartCMBatch(activeBatch);
                Document targetDoc = null;
                string bcRegEx = "";
                string docTypeShort = "";
                bool isFirstDocument = true;
                bool batchNeedsValidation = false;
                
                List<string> docTypeBCTranslationTable;

                // Load settings
                Log.Debug("Loading settings");
                Dictionary<string, string> settings = GetSetupCustomStorageStrings(smartBatch.KCRuntime, smartBatch.KCSetup);
                if (settings.Count == 0) { throw new Exception("Für das CustomModule '" + ServiceName + "' wurden keine Einstellungen gefunden. Bitte konfigurieren Sie das CustomModule für diese Stapelklasse im KC Verwaltungsmodul"); }
                // Get suggested values of translation batch field
                docTypeBCTranslationTable = smartBatch.Setup.FieldTypes.FirstOrDefault(fT => fT.Name == smartBatch.Setup.BatchClass.BatchFieldDefinitions.FirstOrDefault(bF => bF.Name == settings["SmartCAP.CM.TK_BarcodeScans.cboBFDokArtBCTranslation"]).FieldTypeName).SuggestedValues;
                if (docTypeBCTranslationTable == null) { throw new Exception("Kann die DocTypeBCTranslationTable nicht finden! - Bitte vervollständigen Sie die Einrichtung im Verwaltungsmodul."); }
                // get BCRegEx from translation table
                bcRegEx = docTypeBCTranslationTable.FirstOrDefault(t => t.Split(";".ToCharArray()[0]).Contains(smartBatch.BatchFields.FirstOrDefault(bF => bF.Name == settings["SmartCAP.CM.TK_BarcodeScans.cboDocTypeSource"]).Value)).Split(";".ToCharArray()[0])[2];
                if (bcRegEx == null) { throw new Exception("Kann die Barcode-RegEx zum Dokumententyp nicht finden! - Bitte vervollständigen Sie die Einrichtung im Verwaltungsmodul."); }
                // Translate DocType
                docTypeShort = docTypeBCTranslationTable.FirstOrDefault(t => t.Split(";".ToCharArray()[0]).Contains(smartBatch.BatchFields.FirstOrDefault(bF => bF.Name == settings["SmartCAP.CM.TK_BarcodeScans.cboDocTypeSource"]).Value)).Split(";".ToCharArray()[0])[0];
                if (docTypeShort == null) { throw new Exception("Kann die Dokumententyp-Kurzbezeichnung nicht auflösen! - Bitte vervollständigen Sie die Einrichtung im Verwaltungsmodul."); }


                // Separate documents using barcode syntax from translation table
                foreach (Document doc in smartBatch.Documents)
                {
                    DocumentOpened?.Invoke(this, new ACDataElementEventArgs(doc.KCDocument));

                    if (isFirstDocument)        // Check first document for valid barcode, make first document a target doc
                    {
                        targetDoc = doc;        // First document in batch is a target doc for sure

                        if (doc.Pages[0].PageLevelBarcodes.Any(bc => Regex.IsMatch(bc.Value, bcRegEx)))
                        {
                            doc.IndexFields.First(iF => iF.Name == settings["SmartCAP.CM.TK_BarcodeScans.cboBarcodeTarget"]).Value = doc.Pages[0].PageLevelBarcodes.First(bc => Regex.IsMatch(bc.Value, bcRegEx)).Value;
                            if (bool.Parse(settings["SmartCAP.CM.TK_BarcodeScans.chkSetValidDocumentsValidated"])) { doc.Validated = true; }
                        }
                        else
                        {
                            doc.Validated = false;
                            batchNeedsValidation = true;
                        }

                        // Set IndexField with short docType
                        doc.IndexFields.First(iF => iF.Name == settings["SmartCAP.CM.TK_BarcodeScans.cboDocTypeTarget"]).Value = docTypeShort;

                        isFirstDocument = false;
                    }
                    
                    else if (doc.Pages[0].PageLevelBarcodes.Any(bc => Regex.IsMatch(bc.Value, bcRegEx)))    // Check for separation barcode
                    {
                        targetDoc = doc;
                        doc.IndexFields.First(iF => iF.Name == settings["SmartCAP.CM.TK_BarcodeScans.cboBarcodeTarget"]).Value = doc.Pages[0].PageLevelBarcodes.First(bc => Regex.IsMatch(bc.Value, bcRegEx)).Value;

                        if (bool.Parse(settings["SmartCAP.CM.TK_BarcodeScans.chkSetValidDocumentsValidated"])) { doc.Validated = true; }

                        // Set IndexField with short docType
                        doc.IndexFields.First(iF => iF.Name == settings["SmartCAP.CM.TK_BarcodeScans.cboDocTypeTarget"]).Value = docTypeShort;
                    }

                    else
                    {
                        if (targetDoc != null)       // Move doc pages to document that has separation barcode
                        {
                            doc.Pages.ForEach(p => p.MoveToDocument(targetDoc));
                            doc.Delete();
                        }
                        else    // If we dont have a targetDoc, there is something wrong
                        {
                            doc.Rejected = true;
                            doc.Note = "Es gibt kein TargetDoc im Stapel!";
                        }
                    }

                    DocumentClosed?.Invoke(this, new ACDataElementEventArgs(doc.KCDocument));
                }


                // CLose batch, skip validation if possible
                if (batchNeedsValidation)
                { activeBatch.BatchClose(KfxDbState.KfxDbBatchReady, KfxDbQueue.KfxDbQueueNext, 0, ""); }
                else
                {
                    if (smartBatch.Modules.Any(m => m.ID == "index.exe")) { smartBatch.Close(KfxDbState.KfxDbBatchReady, smartBatch.Modules.SkipWhile(m => m.ID != "index.exe").Skip(1).First()); }
                    else { activeBatch.BatchClose(KfxDbState.KfxDbBatchReady, KfxDbQueue.KfxDbQueueNext, 0, ""); }
                }

                BatchClosed?.Invoke(this, new BatchEventArgs(activeBatch));
            }
            catch (Exception ex)
            {
                string message = "Error processing batch: " + ex.ToString();
                Log.Error(message, ex);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
                if (activeBatch != null)
                {
                    activeBatch.BatchClose(KfxDbState.KfxDbBatchError, KfxDbQueue.KfxDbQueueException, 1000, ex.ToString());
                }
            }
        }


        #region processing methods                  

        /// <summary>
        /// This function handles the BatchAvailable event from Kofax Capture. 
        /// This handler attempts to process as many batches as possible.
        /// A mutex is implemented to ensure that this logic is processed by only one thread at a time.
        /// </summary>
        private void Session_BatchAvailable()
        {
            Log.Debug("Session_BatchAvailable");
            // lock access to this call
            mutex.WaitOne();

            ProcessBatches();

            // release the kraken - ah, mutex
            mutex.ReleaseMutex();
        }

        /// <summary>
        /// This function processes all new batches whenever the poll timer elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Log.Debug("PollTimer_Elapsed");

            // lock access to this call
            mutex.WaitOne();

            ProcessBatches();

            // release the kraken - ah, mutex
            mutex.ReleaseMutex();
        }

        /// <summary>
        /// Processes only one specific batch.
        /// </summary>
        /// <param name="batchId"></param>
        public void ProcessSpecificBatch(int batchId)
        {
            try
            {
                activeBatch = session.BatchOpen(batchId, session.ProcessID);
                Log.Info("opened specific batch with id " + batchId);
                ProcessActiveBatch();
            }
            catch (Exception ex)
            {
                string message = "Error opening batch: " + ex.ToString();
                Log.Error(message);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
            }

        }

        /// <summary>
        /// Automatically gets the next batch and processes it.
        /// Returns true, if the batch was processed successful and false, if there are no more batches or an error occured.
        /// </summary>
        /// <returns></returns>
        private bool ProcessNextBatch()
        {
            try
            {
                activeBatch = session.NextBatchGet(login.ProcessID, filter, states);
                if (activeBatch == null)
                {
                    Log.Info("no more batches available at the moment");
                    return false;
                }
                else
                {
                    Log.Info("opened next batch with id " + activeBatch.BatchId);
                    // here happens the action
                    ProcessActiveBatch();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string message = "Error opening next batch: " + ex.ToString();
                Log.Error(message, ex);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
                return false;
            }
        }

        /// <summary>
        /// Automatically processes all batches until there is no one left, or an error occured.
        /// </summary>
        public void ProcessBatches()
        {
            bool processedSuccessful = true;
            while (processedSuccessful)
            {
                processedSuccessful = ProcessNextBatch();
                //Log.Info("processed batch successfully.");
            }
        }

        #endregion login and processing methods

        #region helpers

        /// <summary>
        /// Get a list of all custom storage strings that were set during setup in administration
        /// </summary>
        /// <param name="runtime"></param>
        /// <param name="setup"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetSetupCustomStorageStrings(IACDataElement runtime, IACDataElement setup)
        {
            Dictionary<string, string> customStorageStrings = new Dictionary<string, string>();
            IACDataElement batch = runtime.FindChildElementByName("Batch");
            IACDataElementCollection batchClasses = setup.FindChildElementByName("BatchClasses").FindChildElementsByName("BatchClass");
            foreach (IACDataElement batchClass in batchClasses)
            {
                if (batchClass["Name"] == batch["BatchClassName"])
                {
                    foreach (IACDataElement css in batchClass.FindChildElementByName("BatchClassCustomStorageStrings").FindChildElementsByName("BatchClassCustomStorageString"))
                    {
                        customStorageStrings.Add(css["Name"], css["Value"]);
                    }
                }
            }
            return customStorageStrings;
        }

        #endregion helpers
    }
}
