﻿using Kofax.Capture.DBLite;
using Kofax.Capture.SDK.CustomModule;
using Kofax.Capture.SDK.Data;
using log4net;
using Microsoft.Win32;
using SmartCAP.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SmartCAP.SmartKCBatch;

namespace SmartCAP.CM.TK_Personaldokumente
{
    #region event handlers
    public class BatchEventArgs : EventArgs
    {
        public IBatch Batch { get; private set; }
        public BatchEventArgs(IBatch batch)
        {
            Batch = batch;
        }
    }
    public class ACDataElementEventArgs : EventArgs
    {
        public IACDataElement Element { get; private set; }

        public ACDataElementEventArgs(IACDataElement element)
        {
            Element = element;
        }
    }
    public class TextEventArgs : EventArgs
    {
        public string Text { get; private set; }

        public TextEventArgs(string text)
        {
            Text = text;
        }
    }

    public delegate void BatchEventHandler(object sender, BatchEventArgs e);
    public delegate void ACDataElementEventHandler(object sender, ACDataElementEventArgs e);
    public delegate void TextEventHandler(object sender, TextEventArgs e);
    #endregion event handlers

    public class CustomModule
    {
        #region log4net
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private void InitializeLogging()
        {
            try
            {
                LoggerFunctions.Initialize(this.GetType().Namespace);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not initialize logging: " + ex.Message);
            }
        }
        #endregion        

        public static bool AllowMultiInstance = true;
        public static string ServiceName = "SmartCAP.CM.TK_Personaldokumente";
        public static string DisplayName = "SmartCAP.CM.TK_Personaldokumente Service";
        public static string Description = "Personaldokumente - Erkennt Start- und Stop-Trennblätter, bildet Dokumente";
        /// <summary>
        /// This setting is only used, if batch notification is disabled
        /// </summary>
        private static double pollIntervalSeconds = 60;

        #region initialization and login

        private Login login;
        private IRuntimeSession session;
        private IBatch activeBatch;
        private Mutex mutex;
        private KfxDbFilter filter = KfxDbFilter.KfxDbFilterOnProcess | KfxDbFilter.KfxDbFilterOnStates | KfxDbFilter.KfxDbSortOnPriorityDescending;
        private KfxDbState states = KfxDbState.KfxDbBatchReady;
        private System.Timers.Timer pollTimer = new System.Timers.Timer();

        public event BatchEventHandler BatchOpened;
        public event BatchEventHandler BatchClosed;
        public event ACDataElementEventHandler DocumentOpened;
        public event ACDataElementEventHandler DocumentClosed;
        public event TextEventHandler ErrorOccured;

        /// <summary>
        /// Returns the batch notification state (existence of DisableBatchNotification registry key)
        /// </summary>
        public static bool BatchNotificationEnabled { get { return GetBatchNotificationState(); } }
        private static bool GetBatchNotificationState()
        {
            string registryKey;
            if (Environment.Is64BitOperatingSystem)
                registryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0";
            else
                registryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Kofax Image Products\Ascent Capture\3.0";

            string value = Convert.ToString(Registry.GetValue(registryKey, "DisableBatchNotification", ""));

            if (String.IsNullOrEmpty(value))
            {
                // DisableBatchNotification key does not exist - batch notification is enabled
                return true;
            }
            else
            {
                // DisableBatchNotificationKey exists - batch notification is disabled
                return false;

            }
        }

        /// <summary>
        /// Provides all necessary methods for batch processing
        /// </summary>
        public CustomModule()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, eventArgs) => AssemblyResolver.Resolve(eventArgs);
            InitializeLogging();
            mutex = new Mutex();
        }

        /// <summary>
        ///  Login to KC
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        public void Login(string user, string password, bool showDialog = false)
        {
            login = new Login();
            try
            {
                login.EnableSecurityBoost = true;
                login.Login();
                login.ApplicationName = "SmartCAP.CM.TK_Personaldokumente";
                //login.Version = "1.0";            
                login.ValidateUser("SmartCAP.CM.TK_Personaldokumente.exe", showDialog, user, password);

                session = login.RuntimeSession;
            }
            catch (Exception ex)
            {
                string message = "Error logging in to KC: " + ex.ToString();
                Log.Error(message);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
                throw;
            }
        }

        /// <summary>
        /// The CustomModule will be notified from Kofax Capture, if new batches are available. 
        /// </summary>
        public void ListenForNewBatches()
        {
            if (session == null)
                throw new Exception("you need to login to KC first");

            session.BatchAvailable += Session_BatchAvailable;
        }

        /// <summary>
        /// Polls for new batches
        /// </summary>
        /// <param name="interval">poll interval in milliseconds</param>
        public void PollForNewBatches()
        {
            pollTimer.Interval = pollIntervalSeconds * 1000;
            pollTimer.Elapsed += PollTimer_Elapsed;
            pollTimer.Enabled = true;
        }

        #endregion initialization and login

        /// <summary>
        /// Processes the currently active batch.
        /// Here all the custom magic happens.
        /// </summary>
        private void ProcessActiveBatch()
        {
            try
            {
                BatchOpened?.Invoke(this, new BatchEventArgs(activeBatch));

                SmartCMBatch smartBatch = new SmartCMBatch(activeBatch);
                Document groupDoc = null;
                Document targetDoc = null;
                string startBarcode = "MPSTART";
                string stopBarcode = "MPSTOP";

                
                // Check each doc in batch for start or stop barcode. If none, leave document as it is
                foreach (Document doc in smartBatch.Documents)
                {
                    DocumentOpened?.Invoke(this, new ACDataElementEventArgs(doc.KCDocument));

                    // Check for start and stop barcode
                    if (doc.IndexFields.First(iF => iF.Name == "Barcode").Value == startBarcode)
                    {
                        doc.Pages[0].Delete();
                        doc.IndexFields.First(iF => iF.Name == "Barcode").Value = "";
                        groupDoc = doc;
                        targetDoc = null;
                    }

                    else if (doc.IndexFields.First(iF => iF.Name == "Barcode").Value == stopBarcode)
                    {
                        groupDoc = null;
                        targetDoc = null;
                        doc.Delete();
                    }

                    else if (groupDoc == null && doc.Pages.Count == 1 && doc.Pages[0].Parameters.First(p => p.Name == "Front").Value == "1")   // If we´re having a new front page create new target doc so that a back page will be moved to its front page
                    {
                        targetDoc = doc;
                    }

                    else
                    {
                        if (groupDoc != null)       // If we´re within a group document, move doc pages there
                        {
                            doc.Pages.ForEach(p => p.MoveToDocument(groupDoc));
                            doc.Delete();
                        }
                        else if (targetDoc != null && doc.Pages.Count == 1 && doc.Pages[0].Parameters.First(p => p.Name == "Front").Value == "0")     // If we´re having a first page document and this is not a first page, move pages there
                        {
                            doc.Pages.ForEach(p => p.MoveToDocument(targetDoc));
                            doc.Delete();
                        }
                    }

                    DocumentClosed?.Invoke(this, new ACDataElementEventArgs(doc.KCDocument));
                }


                // Close the batch  
                smartBatch.Close(KfxDbState.KfxDbBatchReady, KfxDbQueue.KfxDbQueueNext);
                
                BatchClosed?.Invoke(this, new BatchEventArgs(activeBatch));
            }
            catch (Exception ex)
            {
                string message = "Error processing batch: " + ex.ToString();
                Log.Error(message, ex);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
                if (activeBatch != null)
                {
                    activeBatch.BatchClose(KfxDbState.KfxDbBatchError, KfxDbQueue.KfxDbQueueException, 1000, ex.ToString());
                }
            }
        }


        #region processing methods                  

        /// <summary>
        /// This function handles the BatchAvailable event from Kofax Capture. 
        /// This handler attempts to process as many batches as possible.
        /// A mutex is implemented to ensure that this logic is processed by only one thread at a time.
        /// </summary>
        private void Session_BatchAvailable()
        {
            Log.Debug("Session_BatchAvailable");
            // lock access to this call
            mutex.WaitOne();

            ProcessBatches();

            // release the kraken - ah, mutex
            mutex.ReleaseMutex();
        }

        /// <summary>
        /// This function processes all new batches whenever the poll timer elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Log.Debug("PollTimer_Elapsed");

            // lock access to this call
            mutex.WaitOne();

            ProcessBatches();

            // release the kraken - ah, mutex
            mutex.ReleaseMutex();
        }

        /// <summary>
        /// Processes only one specific batch.
        /// </summary>
        /// <param name="batchId"></param>
        public void ProcessSpecificBatch(int batchId)
        {
            try
            {
                activeBatch = session.BatchOpen(batchId, session.ProcessID);
                Log.Info("opened specific batch with id " + batchId);
                ProcessActiveBatch();
            }
            catch (Exception ex)
            {
                string message = "Error opening batch: " + ex.ToString();
                Log.Error(message);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
            }

        }

        /// <summary>
        /// Automatically gets the next batch and processes it.
        /// Returns true, if the batch was processed successful and false, if there are no more batches or an error occured.
        /// </summary>
        /// <returns></returns>
        private bool ProcessNextBatch()
        {
            try
            {
                activeBatch = session.NextBatchGet(login.ProcessID, filter, states);
                if (activeBatch == null)
                {
                    Log.Info("no more batches available at the moment");
                    return false;
                }
                else
                {
                    Log.Info("opened next batch with id " + activeBatch.BatchId);
                    // here happens the action
                    ProcessActiveBatch();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string message = "Error opening next batch: " + ex.ToString();
                Log.Error(message, ex);
                ErrorOccured?.Invoke(this, new TextEventArgs(message));
                return false;
            }
        }

        /// <summary>
        /// Automatically processes all batches until there is no one left, or an error occured.
        /// </summary>
        public void ProcessBatches()
        {
            bool processedSuccessful = true;
            while (processedSuccessful)
            {
                processedSuccessful = ProcessNextBatch();
                //Log.Info("processed batch successfully.");
            }
        }

        #endregion login and processing methods

        #region helpers

        /// <summary>
        /// Get a list of all custom storage strings that were set during setup in administration
        /// </summary>
        /// <param name="runtime"></param>
        /// <param name="setup"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetSetupCustomStorageStrings(IACDataElement runtime, IACDataElement setup)
        {
            Dictionary<string, string> customStorageStrings = new Dictionary<string, string>();
            IACDataElement batch = runtime.FindChildElementByName("Batch");
            IACDataElementCollection batchClasses = setup.FindChildElementByName("BatchClasses").FindChildElementsByName("BatchClass");
            foreach (IACDataElement batchClass in batchClasses)
            {
                if (batchClass["Name"] == batch["BatchClassName"])
                {
                    foreach (IACDataElement css in batchClass.FindChildElementByName("BatchClassCustomStorageStrings").FindChildElementsByName("BatchClassCustomStorageString"))
                    {
                        customStorageStrings.Add(css["Name"], css["Value"]);
                    }
                }
            }
            return customStorageStrings;
        }

        #endregion helpers
    }
}
