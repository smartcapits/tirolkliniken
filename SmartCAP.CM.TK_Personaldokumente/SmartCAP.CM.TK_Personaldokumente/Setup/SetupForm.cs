﻿using Kofax.Capture.AdminModule.InteropServices;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;

namespace SmartCAP.CM.TK_Personaldokumente
{
    public partial class SetupForm : Form
    {
        #region Initialization
        public IBatchClass BatchClass;

        public SetupForm()
        {
            InitializeComponent();
        }

        public DialogResult ShowDialog(IBatchClass batchClass)
        {
            BatchClass = batchClass;
            InitializePanel();
            LoadSettings();

            return this.ShowDialog();
        }

        public void InitializePanel()
        {
            // Load all available BatchFields into combo boxes
            foreach (BatchField bf in BatchClass.BatchFields)
            {
                cboBFDokArtTranslation.Items.Add(bf.Name);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (VerifySettings())
            {
                SaveSettings();
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        public void LoadSettings()
        {
            // load all CustomProperties into textboxes
            GetAll(this, typeof(ComboBox)).ToList().ForEach(x => x.Text = GetCustomStorageString("SmartCAP.CM.TK_Personaldokumente." + x.Name));
        }

        public bool VerifySettings()
        {
            if (String.IsNullOrEmpty(cboBFDokArtTranslation.Text))
            {
                MessageBox.Show("Bitte Stapelfeld für Übersetzungstabelle angeben!");
                return false;
            }

            return true;
        }

        public void SaveSettings()
        {
            // save all textbox values to CustomProperties named after the textbox name
            GetAll(this, typeof(ComboBox)).ToList().ForEach(x => SetCustomStorageString("SmartCAP.CM.TK_Personaldokumente." + x.Name, x.Text));
        }

        #region helpers
        /// <summary>
        /// Gets all controls of the specified type recursive (e.g. if you use textboxes inside groupboxes)
        /// </summary>
        /// <param name="control"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
        private void SetCustomStorageString(string name, string value)
        {
            try
            {
                BatchClass.set_CustomStorageString(name, value);
            }
            catch
            {
            }
        }

        private string GetCustomStorageString(string name)
        {
            try
            {
                return BatchClass.get_CustomStorageString(name);
            }
            catch
            {
                return "";
            }
        }

        #endregion
    }
}
