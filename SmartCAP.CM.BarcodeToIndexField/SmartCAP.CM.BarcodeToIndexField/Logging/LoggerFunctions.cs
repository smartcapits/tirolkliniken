﻿using log4net.Config;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCAP.Logging
{
    public class LoggerFunctions
    {
        public static void Initialize(string appName)
        {
            string key = Environment.Is64BitOperatingSystem ?
                @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Kofax Image Products\Ascent Capture\3.0" :
                @"HKEY_LOCAL_MACHINE\SOFTWARE\Kofax Image Products\Ascent Capture\3.0";

            string captureSVPath = Registry.GetValue(key, "ServerPath", "n/a").ToString();
            string configFilePath = Path.Combine(captureSVPath, "Logs", "Config", appName + ".log4net.config");

            //Schau ob es das Verzeichniss gibt wo die Config liegen sollte, falls nicht bau die Ordner
            if (!Directory.Exists(Path.GetDirectoryName(configFilePath)))
                Directory.CreateDirectory(Path.GetDirectoryName(configFilePath));

            //Schau ob es ein Config File gibt, falls nicht erstelle es mit den Standardwerten
            if (!File.Exists(configFilePath))
                File.WriteAllText(configFilePath, buildLogConfig(captureSVPath, appName));

            XmlConfigurator.Configure(new FileInfo(configFilePath));

        }

        private static string buildLogConfig(string captureSVPath, string appName)
        {
            string logFilePath = Path.Combine(captureSVPath, "Logs", appName + ".log");

            if (!Directory.Exists(Path.GetDirectoryName(logFilePath)))
                Directory.CreateDirectory(Path.GetDirectoryName(logFilePath));

            string defaultConfig = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "Logging", "log4net.default.config");

            return File.ReadAllText(defaultConfig).Replace("{0}", appName).Replace("{1}", logFilePath);
            //return String.Format(File.ReadAllText(defaultConfig), appName, logFilePath); // not working somehow
        }

        private static string FormatXml(string xml)
        {
            try
            {
                System.Xml.Linq.XDocument doc = System.Xml.Linq.XDocument.Parse(xml);
                return doc.ToString();
            }
            catch (Exception)
            {
                return xml;
            }
        }
    }
}
